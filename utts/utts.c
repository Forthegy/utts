/* utts.c
 *
 * An updated backend for the speak library, originally written by Roger
 * Merryfield. This code is released under the unlicence.
 * 
 * */

/* TODO
 *
 *   - grab the test from the siphash sample implementation as the reference
 *     dataset.
 *   - implement hashtable and related tests
 *   - implement threading and related tests (client/server model with two Qs)
 *   - implement dispatch infrastructure
 *
 *          .------> action ->  action  -> daemon -> return queue -> fold  ---.
 *          |        table     dispatch                   |          event    |
 *   --> API call                  |                      |          state <--`
 *          |                      V                      |            |
 *          `----------------> API return <---------------`<-----------`
 *                                 |
 *                                 V
 *
 *   - implement driver select API
 *   - implement immediate API
 *   - implement caching API
 * */

#include "private.h"

#define UTTS_TEST /* Take this out and make it controlled from CMake */

/* encapsulated structures subject to patch changes, not part of the API
 * specification */
#include "system.inl"
#include "io.inl"
#include "error.inl"
#include "thread.inl"
#include "hashtable.inl"
#include "queue.inl"

/* driver parts */
#include "dri/dricommon.inl"
#include "dri/drivoid.inl"
#include "dri/drisapi.inl"
#include "dri/drinvda.inl"

/* driver action table */
#include "dri/atab.inl"

/* API */

/* Start and stop. */
#if !defined(UTTS_HAVE_EXECUTE_API)
#error "The execute API must be implemented!"
#endif

int utts_boot(utts_driver_type_e ePreferred, utts_driver_t **ppDriver)
{
  return _uttserr_stub(__FILE__, __LINE__, uttserrpart_core_ifc, "utts_boot");
}

int utts_halt(utts_driver_t **ppDriver)
{
  return _uttserr_stub(__FILE__, __LINE__, uttserrpart_core_ifc, "utts_halt");
}

/* Cache hint control. */
#if defined(UTTS_HAVE_HINT_API)

/* Prepare a sample ahead of time if available through the driver and if
 * preferred by the driver. */
int utts_cstage(utts_speech_descriptor_t *pSpeech, utts_driver_t *pDriver)
{
  return _uttserr_stub(__FILE__, __LINE__, uttserrpart_core_ifc, "utts_cstage");
}

/* Delete an entry from the cache if it's there. */
int utts_cunstage(utts_speech_descriptor_t *pSpeech, utts_driver_t *pDriver)
{
  return _uttserr_stub(__FILE__, __LINE__,
    uttserrpart_core_ifc, "utts_cunstage");
}

/* Purge the sample cache. */
int utts_cpurge(utts_driver_t *pDriver)
{
  return _uttserr_stub(__FILE__, __LINE__, uttserrpart_core_ifc, "utts_cpurge");
}

#endif

/* Speech control. */
#if defined(UTTS_HAVE_CONTROL_API)

/* Speak a speech sample. Creates a cache entry if preferred by the driver. */
int utts_speak(utts_speech_descriptor_t *pSpeech, utts_driver_t *pDriver)
{
  return _uttserr_stub(__FILE__, __LINE__, uttserrpart_core_ifc, "utts_speak");
}

/* Silence all speech. */
int utts_silence(utts_driver_t *pDriver)
{
  return _uttserr_stub(__FILE__, __LINE__,
    uttserrpart_core_ifc, "utts_silence");
}

/* Is speaking? */
int utts_isspeaking(utts_driver_t *pDriver)
{
  return _uttserr_stub(__FILE__, __LINE__,
    uttserrpart_core_ifc, "utts_isspeaking");
}

#endif

/* test */
#if defined(UTTS_TEST)

static int utts_test_log()
{
  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 1), "Log Test", "%s", "BEGIN\n");

  /* pFileName, lineno, err, pMsgStr, pFormat, ... */
  uttserr_log(__FILE__, __LINE__, 0, "No bits set. This is a test message! No extra formatted message.", NULL);
  uttserr_log(__FILE__, __LINE__, -1, "All bits set. Format string test.", "%s", "It works!\n");

  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 1), "Log Test", "%s", "END\n");

  return 0;
}

static int utts_test_hashtable()
{
  int result;
  long hashResult;

  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 2), "Hashtable Test", "%s", "BEGIN\n");

  result = _utts_hash_key("Hello world!", 12, &hashResult);
  uttserr_log(__FILE__, __LINE__,
    uttserr_pack(0, uttserrcode_info, 2), "Hashtable Test",
    "Hash of \"Hello world!\" -> %ld\n", hashResult);

  result = _utts_hash_key((void *)((char)42), uttskeysize_char, &hashResult);
  uttserr_log(__FILE__, __LINE__,
    uttserr_pack(0, uttserrcode_info, 2), "Hashtable Test",
    "Hash of (char)42 -> %ld\n", hashResult);

  result = _utts_hash_key((void *)((short)42), uttskeysize_short, &hashResult);
  uttserr_log(__FILE__, __LINE__,
    uttserr_pack(0, uttserrcode_info, 2), "Hashtable Test",
    "Hash of (short)42 -> %ld\n", hashResult);

  result = _utts_hash_key((void *)((int)42), uttskeysize_int, &hashResult);
  uttserr_log(__FILE__, __LINE__,
    uttserr_pack(0, uttserrcode_info, 2), "Hashtable Test",
    "Hash of (int)42 -> %ld\n", hashResult);

  result = _utts_hash_key((void *)((long)42), uttskeysize_long, &hashResult);
  uttserr_log(__FILE__, __LINE__,
    uttserr_pack(0, uttserrcode_info, 2), "Hashtable Test",
    "Hash of (long)42 -> %ld\n", hashResult);

  result = _utts_hash_key((void *)((long long)42),
    uttskeysize_longlong, &hashResult);
  uttserr_log(__FILE__, __LINE__,
    uttserr_pack(0, uttserrcode_info, 2), "Hashtable Test",
    "Hash of (long long)42 -> %ld\n", hashResult);

  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 2), "Hashtable Test", "%s", "END\n");

  return 0;
}

static int utts_test_queue()
{
  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 3), "Queue Test", "%s", "BEGIN\n");

  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 3), "Queue Test", "%s", "END\n");

  return 0;
}

static int utts_test_thread()
{
  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 4), "Thread Test", "%s", "BEGIN\n");

  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 4), "Thread Test", "%s", "END\n");

  return 0;
}
static int utts_test_alloc()
{
  int result, result2;
  utts_driver_t *pDriver;

  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 10), "Alloc Test", "%s", "BEGIN\n");

  pDriver = NULL;
  result = _utts_alloc_driver(utts_driver_void_type, sizeof(utts_driver_void_t), &pDriver);
  uttserr_log(__FILE__, __LINE__, result, "driver alloc result", NULL);
  if (0 != result) goto alloctestdone;

  /* TODO add alloc tests for other drivers as they're attended to! */
/*alloctestcleanup:*/
  result2 = _utts_dealloc_driver(utts_driver_void_type, sizeof(utts_driver_void_t), &pDriver);
  uttserr_log(__FILE__, __LINE__, result2, "driver dealloc result", NULL);
  if (0 != result2 && 0 == result) result = result2;

alloctestdone:
  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 10), "Alloc Test", "%s", "END\n");

  return (0 != result);
}

static int utts_test_void()
{
  int result, result2;
  utts_driver_t *pDriver;
  utts_render_arg_t mRenderArg;
  utts_play_arg_t mPlayArg;

  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 20), "Void Driver Test", "%s", "BEGIN\n");

  pDriver = NULL;
  result = _utts_alloc_driver(utts_driver_void_type, sizeof(utts_driver_void_t), &pDriver);
  uttserr_log(__FILE__, __LINE__, result, "driver alloc result", NULL);
  if (0 != result) goto voidtestdone;

  result = _utts_driver_boot(pDriver);
  uttserr_log(__FILE__, __LINE__, result, "driver boot result", NULL);
  if (0 != result) goto voidtestcleanup;

  clearmem(&mRenderArg, sizeof(mRenderArg));
  clearmem(&mPlayArg, sizeof(mPlayArg));

  result = _utts_driver_render(&mRenderArg, pDriver);
  uttserr_log(__FILE__, __LINE__, result, "driver render result", NULL);

  result = _utts_driver_play(&mPlayArg, pDriver);
  uttserr_log(__FILE__, __LINE__, result, "driver play result", NULL);

  result = _utts_driver_halt(pDriver);
  uttserr_log(__FILE__, __LINE__, result, "driver halt result", NULL);
  if (0 != result) goto voidtestcleanup;

voidtestcleanup:
  result2 = _utts_dealloc_driver(utts_driver_void_type, sizeof(utts_driver_void_t), &pDriver);
  uttserr_log(__FILE__, __LINE__, result2, "driver dealloc result", NULL);
  if (0 != result2 && 0 == result) result = result2;

voidtestdone:
  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 20), "Void Driver Test", "%s", "END\n");

  return (0 != result);
}

static int utts_test_sapi()
{
  return 0;
}

static int utts_test_nvda()
{
  return 0;
}

int main()
{
  int result;

  result = 0;

  result |= (0 != utts_test_log());
  result |= (0 != utts_test_hashtable());
  result |= (0 != utts_test_queue());
  result |= (0 != utts_test_thread());
  result |= (0 != utts_test_alloc());
  result |= (0 != utts_test_void());

  uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 0), "All tests", "%s", "END\n");

  if (0 != result)
    uttserr_log(__FILE__, __LINE__, uttserr_pack(0, uttserrcode_info, 0), "Some test(s) failed!", NULL);

  return result;
}

#endif
