#ifndef _UTTS_ERROR_IMPL_
#define _UTTS_ERROR_IMPL_

#include "utts_error.h"

/* Prototypes */

/* error printing stuff */
static int _uttserr_vioprintf(
  struct utts_iospec *pIO, char const *pLibName,
  char const *pFileName, int lineno, int err, char const *pMsgStr,
  char const *pFormat, va_list va);

/* Implementation */
#if defined(UTTS_HAVE_IO_API)

#include <assert.h>

/* error reporting */
static int _uttserr_vioprintf(
  struct utts_iospec *pIO, char const *pLibName,
  char const *pFileName, int lineno, int err, char const *pMsgStr,
  char const *pFormat, va_list va)
{
#if defined(UTTS_HAVE_STDTIME)
  utts_timespec_t ts;
#endif
  struct utts_iospec iospec;
  unsigned long nlibname, nfilename, nmsg;
  int len, part, code, user, enclen, rc;
  char const *pUserPfx;
  void *pTop;

  static char const *msArgErrStr = "arg:";
  static char const *msInfoErrStr = "info:";
  static char const *msConcat = " : ";
  static char const *msEndline = "\n";
  static char const *msErrorFormat = " %s:%d (part=%d code=%d user=%s%d) %s%s";

#if defined(UTTS_HAVE_STDTIME)
  clearmem(&ts, sizeof(ts));
  _utts_timeutc(&ts);
#endif

  nlibname = (unsigned long)strnlen(pLibName, 31);
  nfilename = (unsigned long)strnlen(pFileName, 4095);
  nmsg = (unsigned long)strnlen(pMsgStr, 255);
  len = nlibname /* Library name length. */
    + nfilename  /* Filename length. */
    + nmsg       /* Message string length. */
    + (4*20)     /* four possibly 64-bit ints, 20 digits apiece */
    + 5          /* msInfoErrStr */
    + 28         /* time string */
    + 64         /* msErrorFormat copy content (actually 23). */
    + 1          /* Space after the library name. */
    + 1;         /* null terminator. */

  /* push the stack pointer by len bytes and write the new top address in pTop.
   * */
  pushstack(len, &pTop);
  clearmem(pTop, len);

  memcpy(pTop, pLibName, nlibname);
#if defined(UTTS_HAVE_STDTIME)
  ((char *)pTop)[nlibname++] = ' '; /* add a space */
  rc = _utts_timeutctoa((char *)pTop + nlibname, len - nlibname, &ts);
  if (rc <= 0)
  {
    ((char *)pTop)[nlibname] = (char)0;
    rc = 0;
  }
  rc += nlibname;
#else
  rc = nlibname;
#endif

  part = uttserr_getpart(err);
  code = uttserr_getcode(err);
  
  if (uttserr_isargerr(err))
  {
    pUserPfx = msArgErrStr;
    user = uttserr_getarg(err);
  }
  else
  {
    pUserPfx = msInfoErrStr;
    user = uttserr_getuser(err);
  }

  clearmem(&iospec, sizeof(struct utts_iospec));
  iospec.kind = uttsio_stdiobuf;
  iospec.pData = (char *)pTop + rc;
  iospec.sz = len - rc;

  enclen = utts_ioprintf(&iospec, msErrorFormat,
    pFileName, lineno, part, code, pUserPfx, user, pMsgStr,
    NULL == pFormat ? msEndline : msConcat);

  /* Ensure we didn't spill past the buffer limit or encounter any encoding
   * errors. */
  assert(enclen >= 0 && enclen < len - rc);

  rc += enclen;

  /* Do not allow a buffer overflow situation. */
  clearmem((char *)pTop + rc, len - rc);
  utts_ioprintf(pIO, "%s", (char *)pTop);

  if (NULL != pFormat)
  {
    /* -6 lines from error printing */
    len = utts_vioprintf(pIO, pFormat, va); 
    if (len >= 0)
      return enclen + len;
    else
    {
      utts_ioprintf(pIO, "%s%s:%d" "%s%d" "%s%s%s",
        "\nuTTS ", __FILE__, __LINE__ - 6, /* error printing */
        " encountered encoding error ", len,
        " writing formatted output : ", pFormat, "\n");
     return len;
    }
  }

  return enclen;
}
#else
static int _uttserr_vioprintf(
  struct utts_iospec *pIO, char const *pLibName,
  char const *pFileName, int lineno, int err, char const *pMsgStr,
  char const *pFormat, va_list va)
{
  volatile int temp;

  (void)(pFileName, lineno, err, pMsgStr, pFormat, va); /* ignore */
  temp = 0xA55AC33C; /* mark the top of the stack */

  return 0;
}
#endif

static int bgerriospec_init = 0;
static struct utts_iospec mgerrio = { 0 };

int uttserr_setiospec(struct utts_iospec *pIO)
{
  if (NULL == pIO)
    return -1;

  clearmem(&mgerrio, sizeof(struct utts_iospec));
  memcpy(&mgerrio, pIO, sizeof(struct utts_iospec));
  bgerriospec_init = -1;

  return 0;
}

static void uttserr_ensureiospec(void)
{
#  define _MAGIC 0xA55AC33C
  if (bgerriospec_init != _MAGIC)
  {
    bgerriospec_init = 0;

    clearmem(&mgerrio, sizeof(struct utts_iospec));
#if !defined(UTTS_NOSTDIO) && !defined(UTTS_SILENT)
    mgerrio.kind = uttsio_stdiofile;
    mgerrio.pData = (void *)stdout;
    mgerrio.sz = 0;
#endif

    bgerriospec_init = _MAGIC;
  }
#   undef _MAGIC
}

int uttserr_getiospec(struct utts_iospec *pIO)
{
  if (NULL == pIO)
    return -1;

  (void)uttserr_ensureiospec();

  clearmem(pIO, sizeof(struct utts_iospec));
  memcpy(pIO, &mgerrio, sizeof(struct utts_iospec));

  return 0;
}

/* pFileName, lineno, err, pMsgStr, pFormat, ... */
int uttserr_log(
  char const *pFN, int lineno,
  int err, char const *pMsgStr,
  char const *pFormat, ...)
{
  va_list va;
  int result;
  struct utts_iospec mIO;

  (void)uttserr_getiospec(&mIO);

  va_start(va, pFormat);
  result = _uttserr_vioprintf(
    &mIO, "uTTS", pFN, lineno, err, pMsgStr, pFormat, va);
  va_end(va);

  return result;
}


#endif