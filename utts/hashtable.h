#ifndef _UTTS_HASHTABLE_INCLUDED_
#define _UTTS_HASHTABLE_INCLUDED_

#include "system.h"

#define UTTS_HASHTABLE_MAX_KEYSIZE 1024

/* Hashtable entry */
typedef struct utts_hashtable_entry
{
  void *pKey;   /* an int of any size or pointer to buffer. */
  long keySize; /* size of the key data, -n for n = 1 (char), 2 (short),
                 * 3 (int), 4 (long), or 5 (long long), or 0 if the entry is
                 * empty. */

  void *pData;  /* address of data (open to user interpretation). */
} utts_hashtable_entry_t;

/* Hashtable */
typedef struct utts_hashtable
{
  utts_hashtable_entry_t *pFirstEntry; /* Address of the first hashtable entry.
                                        * */
  long size;                           /* Number of entries in the current
                                        * table. */
} utts_hashtable_t;

/* Special enum values for the key size. All special values of the key size are
 * non-positive, all positive values of the key size indicate the size of the
 * stored key data in bytes, and in that case pKey points to a buffer that is
 * expected to be allocated and deallocated by the hashtable implementation. */
enum
{
  /* This enum indicates that no key is stored. The hash value associated with
   * an absent key is undefined. */
  uttskeysize_none     =  0L,

  /* Each of these enums indicates that the key is an integer stored in the same
   * memory as thekey address by way of a cast to a long long and then to a void
   * pointer. */
  uttskeysize_char     = -1L,
  uttskeysize_short    = -2L,
  uttskeysize_int      = -3L,
  uttskeysize_long     = -4L,
  uttskeysize_longlong = -5L
};

static inline int _utts_hash_key(void const *, long, long *);

static int _utts_hashtable_entry_vclear(long, utts_hashtable_entry_t *);
static int _utts_hashtable_entry_vdone(long, utts_hashtable_entry_t *);

static int _utts_hashtable_entry_init(
  void const *, long, void *, utts_hashtable_entry_t *);

static int utts_hashtable_init(utts_hashtable_t *);
static int utts_hashtable_done(utts_hashtable_t *);

static int utts_hashtable_insert(
  void const *, long , void *, utts_hashtable_t *);
static int utts_hashtable_delete(void const *, utts_hashtable_t *);
static int utts_hashtable_rehash(long , utts_hashtable_t *);

#endif