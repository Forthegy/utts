#ifndef _UTTS_QUEUE_INCLUDED_
#define _UTTS_QUEUE_INCLUDED_

#include "system.h"

/* queue entry structure */
typedef struct utts_queue_entry
{
  utts_speech_descriptor_t mSpeechDesc;
  struct utts_queue_entry *pNextEntry;
} utts_queue_entry_t;

/* queue structure */
typedef struct utts_queue
{
  utts_queue_entry_t *pHeadEntry;
  utts_queue_entry_t *pTailEntry;
  unsigned long nq;
} utts_queue_t;

/* queue control */
static int _utts_queue_init(utts_queue_t *);
static int _utts_queue_done(utts_queue_t *);
static int _utts_queue_push(utts_speech_descriptor_t const *, utts_queue_t *);
static int _utts_queue_pop(utts_speech_descriptor_t *, utts_queue_t *);

#endif
