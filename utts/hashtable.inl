#ifndef _UTTS_HASHTABLE_IMPL_
#define _UTTS_HASHTABLE_IMPL_

#include "hashtable.h"
#include "siphash/sip.c"

/* If uTTS is used as a high load distributed cache layer for an on-line TTS
 * service, it might be wise to dynamically initialize this in each instance. */
static char seed[8] = { 0x31, 0x33, 0x35, 0x37, 0x39, 0x30, 0x32, 0x34 };

static inline int _utts_hash_key(
  void const *pKey, long keySize, long *pResult)
{
  if (NULL == pResult)
    return uttserr_argpack(uttserrpart_hashtable, uttserrcode_null_argument, 3);

  switch (keySize)
  {
  case uttskeysize_none:
    return uttserr_argpack(uttserrpart_hashtable, uttserrcode_bad_argument, 2);
    break;
  case uttskeysize_char:
    {
      char key;
      key = (char)((long long)pKey);
      (void)_halfsiphash(&key, sizeof(char), &seed[0],
        (char *)((void *)pResult), sizeof(long));
    }
    break;
  case uttskeysize_short:
    {
      short key;
      key = (short)((long long)pKey);
      (void)_halfsiphash(&key, sizeof(short), &seed[0],
        (char *)((void *)pResult), sizeof(long));
    }
    break;
  case uttskeysize_int:
    {
      int key;
      key = (int)((long long)pKey);
      (void)_halfsiphash(&key, sizeof(int), &seed[0],
        (char *)((void *)pResult), sizeof(long));
    }
    break;
  case uttskeysize_long:
    {
      long key;
      key = (long)((long long)pKey);
      (void)_halfsiphash(&key, sizeof(long), &seed[0],
        (char *)((void *)pResult), sizeof(long));
    }
    break;
  case uttskeysize_longlong:
    {
      long long key;
      key = (long long)pKey;
      (void)_halfsiphash(&key, sizeof(long long), &seed[0],
        (char *)((void *)pResult), sizeof(long));
    }
    break;
  default:
    if (NULL == pKey)
      return uttserr_argpack(
        uttserrpart_hashtable, uttserrcode_null_argument, 1);
    
    (void)_halfsiphash(pKey, keySize, &seed[0],
      (char *)((void *)pResult), sizeof(long));
    break;
  };
  return 0;
}

static int _utts_hashtable_entry_vclear(
  long count, utts_hashtable_entry_t *pFirstEntry)
{
  clearmem(pFirstEntry, sizeof(utts_hashtable_entry_t) * count);
  return 0;
}

static int _utts_hashtable_entry_vdone(
  long count, utts_hashtable_entry_t *pFirstEntry)
{
  long c;

  if (NULL == pFirstEntry)
    return uttserr_argpack(
      uttserrpart_hashtable, uttserrcode_null_argument, 2);

  if (0 > count)
    return uttserr_argpack(
      uttserrpart_hashtable, uttserrcode_bad_argument, 1);

  c = count;
  while (0 < c--)
  {
    if (uttskeysize_none /* 0L */ < pFirstEntry->keySize)
      (void)free(pFirstEntry->pKey);

    ++pFirstEntry;
  }

  pFirstEntry -= count;
  clearmem(pFirstEntry, sizeof(utts_hashtable_entry_t)*count);
  return 0;
}

#pragma warning(push)
#pragma warning(disable:4312)
/* We disable the warning that a word is being cast to a larger size address. */

/* mask for sign bit */
#define _gsgn(M) ((M) ^ ((M) >> 1))

/* sign extension with precomputed mask */
#define _gsgnx(Value, M) ((((Value) & (M)) ^ _gsgn(M)) - _gsgn(M))

#define _gimask(Type) ((unsigned long long)((unsigned Type)((signed Type)-1)))

static int _utts_hashtable_entry_init(
  void const *pKey, long keySize, void *pData, utts_hashtable_entry_t *pEntry)
{
  if (NULL == pEntry)
    return uttserr_argpack(
      uttserrpart_hashtable, uttserrcode_null_argument, 4);

  if (0 < pEntry->keySize)
    (void)free(pEntry->pKey);

  clearmem(pEntry, sizeof(utts_hashtable_entry_t));

  switch (keySize)
  {
  case uttskeysize_none:
    return uttserr_argpack(
      uttserrpart_hashtable, uttserrcode_hashtable_invalidkey, 2);
    break;
  case uttskeysize_char:
    pEntry->pKey = (void *)_gsgnx((unsigned long long)pKey, _gimask(char));
    break;
  case uttskeysize_short:
    pEntry->pKey = (void *)_gsgnx((unsigned long long)pKey, _gimask(short));
    break;
  case uttskeysize_int:
    pEntry->pKey = (void *)_gsgnx((unsigned long long)pKey, _gimask(int));
    break;
  case uttskeysize_long:
    pEntry->pKey = (void *)_gsgnx((unsigned long long)pKey, _gimask(long));
    break;
  case uttskeysize_longlong:
    pEntry->pKey = (void *)((long long)pKey);
    break;
  default:
    if (NULL == pKey)
      return uttserr_argpack(
        uttserrpart_hashtable, uttserrcode_null_argument, 1);

    if (keySize > UTTS_HASHTABLE_MAX_KEYSIZE)
      return uttserr_argpack(
        uttserrpart_hashtable, uttserrcode_hashtable_invalidkey, 2);

    pEntry->pKey = malloc(keySize);

    if (NULL == pEntry->pKey)
      return uttserr_pack(
        uttserrpart_hashtable, uttserrcode_out_of_memory, 0);

    (void)memcpy(pEntry->pKey, pKey, keySize);

    break;
  }

  pEntry->pData = pData;
  return 0;
}

#pragma warning(pop)

static int utts_hashtable_init(utts_hashtable_t *pHashTable)
{
  return _uttserr_stub(__FILE__, __LINE__,
    uttserrpart_hashtable, "utts_hashtable_init");
}

static int utts_hashtable_done(utts_hashtable_t *pHashTable)
{
  return _uttserr_stub(__FILE__, __LINE__,
    uttserrpart_hashtable, "utts_hashtable_done");
}

static int utts_hashtable_insert(
  void const *pKey, long keySize, void *pData, utts_hashtable_t *pHashTable)
{
  return _uttserr_stub(__FILE__, __LINE__,
    uttserrpart_hashtable, "utts_hashtable_insert");
}

static int utts_hashtable_delete(void const *pKey, utts_hashtable_t *pHashTable)
{
  return _uttserr_stub(__FILE__, __LINE__,
    uttserrpart_hashtable, "utts_hashtable_delete");
}

static int utts_hashtable_rehash(long capacity, utts_hashtable_t *pHashTable)
{
  return _uttserr_stub(__FILE__, __LINE__,
    uttserrpart_hashtable, "utts_hashtable_rehash");
}

#endif
