#ifndef _UTTS_QUEUE_IMPL_
#define _UTTS_QUEUE_IMPL_

#include "utts.h"
#include "private.h"
#include "queue.h"

/* speech descriptor queue implementation */
static int _utts_queue_init(utts_queue_t *pQueue)
{
  if (NULL == pQueue)
    return uttserr_pack(uttserrpart_queue, uttserrcode_null_argument, -1); /* first argument error */

  clearmem(pQueue, sizeof(utts_queue_t));
  return 0;
}

static int _utts_queue_done(utts_queue_t *pQueue)
{
  utts_queue_entry_t *pNext;

  if (NULL == pQueue)
    return uttserr_pack(uttserrpart_queue, uttserrcode_null_argument, -1); /* first argument error */

  while (NULL != pQueue->pHeadEntry)
  {
    pNext = pQueue->pHeadEntry->pNextEntry;
    free((void *)pQueue->pHeadEntry);
    pQueue->pHeadEntry = pNext;
  }

  clearmem(pQueue, sizeof(utts_queue_t));
  return 0;
}

/* 0 on success, -1, -2 argument errors, -128 out of memory, 2 queue full */
static int _utts_queue_push(
  utts_speech_descriptor_t const *pSpeech, utts_queue_t *pQueue)
{
  utts_queue_entry_t *pEntry;

  if (NULL == pSpeech)
    return uttserr_pack(uttserrpart_queue, uttserrcode_null_argument, -1); /* first argument error */

  if (NULL == pQueue)
    return uttserr_pack(uttserrpart_queue, uttserrcode_null_argument, -2); /* second argument error */

  pEntry = (utts_queue_entry_t *)malloc(sizeof(utts_queue_entry_t));
  if (NULL == pEntry)
    return uttserr_pack(uttserrpart_queue, uttserrcode_out_of_memory, 0); /* out of memory error */

  clearmem((void *)pEntry, sizeof(utts_queue_entry_t));
  memcpy(&pEntry->mSpeechDesc, pSpeech, sizeof(utts_speech_descriptor_t));
  pEntry->pNextEntry = NULL;

  if (NULL == pQueue->pHeadEntry)
    pQueue->pHeadEntry = pQueue->pTailEntry = pEntry;
  else
  {
    pQueue->pTailEntry->pNextEntry = pEntry;
    pQueue->pTailEntry = pEntry;
  }

  return 0;
}

static int _utts_queue_pop(
  utts_speech_descriptor_t *pSpeech, utts_queue_t *pQueue)
{
  utts_queue_entry_t *pHead;

  if (NULL == pSpeech)
    return uttserr_pack(uttserrpart_queue, uttserrcode_null_argument, -1); /* first argument error */

  if (NULL == pQueue)
    return uttserr_pack(uttserrpart_queue, uttserrcode_null_argument, -2); /* second argument error */

  if (NULL == (pHead = pQueue->pHeadEntry))
    return uttserr_pack(uttserrpart_queue, uttserrcode_queue_empty, 0); /* queue empty error */

  pQueue->pHeadEntry = pHead->pNextEntry;
  if (NULL == pQueue->pHeadEntry)
    pQueue->pTailEntry = NULL;

  clearmem(pSpeech, sizeof(utts_speech_descriptor_t));
  memcpy(pSpeech, &pHead->mSpeechDesc, sizeof(utts_speech_descriptor_t));

  free((void *)pHead);
  --pQueue->nq;

  return 0;
}


#endif
