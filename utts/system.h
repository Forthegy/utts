#ifndef _UTTS_SYSTEM_INCLUDED_
#define _UTTS_SYSTEM_INCLUDED_

#include "utts.h"

#if defined(UTTS_WINDOWS)
#define COBJMACROS
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <sapi.h>
#include <ole2.h>
#endif

#include <malloc.h>

#if !defined(UTTS_NOSTDIO)
#include <stdio.h>
#define _print_stack_overflow \
  fprintf(stderr, "%s", "uTTS stack overflow; halt!\n");
#else
#define _print_stack_overflow
#endif

#if defined(UTTS_WINDOWS)
#define pushstack(Count, ppData)                        \
{                                                       \
  __try                                                 \
    { *(ppData) = _alloca((Count)); }                   \
  __except(STATUS_STACK_OVERFLOW == GetExceptionCode()) \
  {                                                     \
    _print_stack_overflow;                              \
    exit(255);                                          \
  };                                                    \
}
#else
#define pushstack(Count, ppData) \
  { *(ppData) = alloca((Count)); }
#endif

#if defined(UTTS_WINDOWS)
#define clearmem(p, sz) ZeroMemory((p), (sz))
#else
#define clearmem(p, sz) memset((p), 0, (sz))
#endif

#if !defined(UTTS_NOSTDTIME)
#define UTTS_HAVE_STDTIME

#include <time.h>

#if !defined(UTTS_WINDOWS)
#include <unistd.h> /* _POSIX_TIMERS */
#ifndef _POSIX_TIMERS
#define _POSIX_TIMERS (-1)
#endif
#if (_POSIX_TIMERS <= 0)
#include <sys/time.h> /* gettimeofday() */
#endif
#endif
#endif

/* get UTC timespec portably between windows and posix. return 0 on success. */
static int _utts_timeutc(utts_timespec_t *);
static int _utts_timeutctoa(char *, unsigned long, utts_timespec_t const *);

#endif