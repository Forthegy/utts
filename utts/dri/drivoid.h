#ifndef _UTTS_DRIVER_VOID_INCLUDED_
#define _UTTS_DRIVER_VOID_INCLUDED_

#include "proto.h"

/* all drivers depend on at least some of the common driver components */
#include "dricommon.h"

/* void driver structure */
typedef struct utts_driver_void
{
  utts_driver_common_t mCommon;
} utts_driver_void_t;

/* void driver address cast */
static utts_driver_void_t *_utts_driver_void_cast(utts_driver_t *);

/* void driver implementation */
static int _utts_driver_void_boot(void const * /* unused */, utts_driver_t *);
static int _utts_driver_void_halt(void const * /* unused */, utts_driver_t *);

static int _utts_driver_void_render(utts_render_arg_t *, utts_driver_t *);
static int _utts_driver_void_play(utts_play_arg_t *, utts_driver_t *);

#endif