#ifndef _UTTS_DRIVER_PROTOTYPES_INCLUDED_
#define _UTTS_DRIVER_PROTOTYPES_INCLUDED_

#include "../private.h"

typedef struct utts_feedq_arg
{
  /*  in: Address of the first speech descriptor to push.
   * out: Address of the first speech descriptor not pushed to queue.
   *      (if any, see nSpeechDesc) */
  utts_speech_descriptor_t const *pSpeechDescArray;

  /*  in: Number of speech descriptors to push to queue.
   * out: Number of speech descriptors not pushed to queue. */
  unsigned long nSpeechDesc;
  unsigned long flags; /* Behavior flags. Currently reserved. */
} utts_feedq_arg_t;

typedef struct utts_pumpq_arg
{
  /* in: Address of the speech descriptor array to write to. */
  utts_speech_descriptor_t *pSpeechDescArray; 

  /*  in: Number of speech descriptors available for writing.
   * out: Number of speech descriptors actually written. */
  unsigned long nSpeechDesc;
  unsigned long flags; /* Behavior flags. Currently reserved. */
} utts_pumpq_arg_t;

enum
{
  /*  in: If this flag is set, the rendered speech audio, if any, will be cached
   *      if the driver supports caching. An internal cache policy of the driver
   *      may override this in the positive; it is not sufficient that the flag
   *      is unset for the audio not to be cached.
   * out: Set if and only if the rendered speech was cached, and unset
   *      otherwise. The driver must always unset this flag if it does not
   *      support caching. */
  utts_renderop_save = 0x0001,

  /*  in: If this flag is set, the cache will be checked before rendering speech
   *      audio whenever the driver supports caching. If an existing entry is
   *      found for the given speech descriptor, it will be immediately
   *      returned, and no rendering will take place.
   * out: Set if and only if the speech audio could be loaded from the cache
   *      immediately and the driver supports caching, and unset otherwise. */
  utts_renderop_load = 0x0002,

  /*  in: If this flag is set in conjunction with the save flag and caching is
   *      supported by the driver, the existing cache entry will be deleted
   *      first and then replaced by the new audio. If this flag is set whenever
   *      the save flag is unset and caching is supported by the driver, the
   *      cache entry is always purged if it exists. If this flag is unset, then
   *      nothing is purged unless an internal cache policy of the driver
   *      overrides the request.
   * out: Set if and only if the audio associated with the given speech
   *      descriptor was deleted from the cache, and unset otherwise. */
  utts_renderop_dlte = 0x0004,

  /*  in: If this flag is set and the save, load, and delete flags are all
   *      unset, then the rendering, cache presence, and playback status of the
   *      speech audio is collected in to the fields of the utts_render_stat
   *      structure addressed in the utts_render_arg structure as pRenderStat.
   *      pRenderStat in this case must be non-null. If any other flags are set
   *      along with this flag, they are ignored.
   * out: Set if and only if the pRenderStat structure was filled in. */
  utts_renderop_stat = 0x0008,

  /* reserved flags (remaining bits in the low part are reserved 4-15) */
  utts_renderop_reserved01 = 0x0010,
  utts_renderop_reserved02 = 0x0020,
  utts_renderop_reserved03 = 0x0040,
  utts_renderop_reserved04 = 0x0080,

  utts_renderop_reserved05 = 0x0100,
  utts_renderop_reserved06 = 0x0200,
  utts_renderop_reserved07 = 0x0400,
  utts_renderop_reserved08 = 0x0800,

  utts_renderop_reserved09 = 0x1000,
  utts_renderop_reserved10 = 0x2000,
  utts_renderop_reserved11 = 0x4000,
  utts_renderop_reserved12 = 0x8000
};

typedef struct utts_render_arg
{
  /*  in: Address of the speech descriptor to render. */
  utts_speech_descriptor_t *pSpeechDesc;

  /* out: A pointer to the structure that contains the audio data. This will
   *      differ depending on the driver, and may in some cases even be unused.
   *      Drivers such as NVDA are performant enough that their in-built control
   *      functions are sufficient when directly used. Drivers such as SAPI need
   *      a special cache layer (pre-loading to mitigate lag) and interaction
   *      with pulse code data directly (deleting leading silences) in order to
   *      provide the same responsiveness, so an ISpStream object handle is
   *      returned. */
  void *pAudioData;

  /* i|o: See utts_renderop enums for documentation. */
  int renderOp;

  unsigned long flags; /* Behavior flags. Currently reserved. */
} utts_render_arg_t;

typedef struct utts_play_arg
{
  struct utts_render_arg *pRenderArgument;

  /*  in: Playback command.
   * out: Zero on success, or a value in the range of */
  int playOp;

  unsigned long flags; /* Behavior flags. Currently reserved. */
} utts_play_arg_t;

/* subroutine prototypes for driver implementation */
typedef int (*pfn_utts_driver_boot_t)(void const * /*unused*/, utts_driver_t *);
typedef int (*pfn_utts_driver_halt_t)(void const * /*unused*/, utts_driver_t *);
typedef int (*pfn_utts_driver_feedq_t)(utts_feedq_arg_t *, utts_driver_t *);
typedef int (*pfn_utts_driver_pumpq_t)(utts_pumpq_arg_t *, utts_driver_t *);
typedef int (*pfn_utts_driver_render_t)(utts_render_arg_t *, utts_driver_t *);
typedef int (*pfn_utts_driver_play_t)(utts_play_arg_t *, utts_driver_t *);

#endif
