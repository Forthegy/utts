#ifndef _UTTS_DRIVER_SAPI_INCLUDED_
#define _UTTS_DRIVER_SAPI_INCLUDED_

#include "proto.h"

#if defined(UTTS_WINDOWS)

/* all drivers depend on at least some of the common driver components */
#include "dricommon.h"

/* Microsoft SAPI driver structure */
typedef struct utts_driver_sapi
{
  utts_driver_common_t mCommon;

  ISpVoice *cpVoice;
  ISpStream *cpStream;
  IStream *cpBaseStream;
  GUID guidWavFormat;
  WAVEFORMATEX wavFormat;
} utts_driver_sapi_t;

/* sapi driver address cast */
static utts_driver_sapi_t *_utts_driver_sapi_cast(utts_driver_t *);

/* sapi driver implementation */
static int _utts_driver_sapi_boot(void const * /* unused */, utts_driver_t *);
static int _utts_driver_sapi_halt(void const * /* unused */, utts_driver_t *);

static int _utts_driver_sapi_render(utts_render_arg_t *, utts_driver_t *);
static int _utts_driver_sapi_play(utts_play_arg_t *, utts_driver_t *);

#endif

#endif