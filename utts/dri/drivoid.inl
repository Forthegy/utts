#ifndef _UTTS_DRIVER_VOID_IMPL_
#define _UTTS_DRIVER_VOID_IMPL_

#include "drivoid.h"

/* TODO: update my return codes. */

/* void driver address cast */
static utts_driver_void_t *_utts_driver_void_cast(utts_driver_t *pDriver)
{
  if (NULL != pDriver && utts_driver_void_type == pDriver->eType)
    return (utts_driver_void_t *)pDriver;
  else
    return NULL;
}

/* void driver implementation */
static int _utts_driver_void_boot(void const *_unused0, utts_driver_t *pDriver)
{
  int result;
  utts_driver_void_t *pDriverVoid;

  (void)_unused0; /* ignore the unused parameter */

  pDriverVoid = _utts_driver_void_cast(pDriver);
  if (NULL == pDriverVoid)
    return uttserr_argpack(
      uttserrpart_driver_void, uttserrcode_bad_argument, 2);

  if (0 != (result = _utts_queue_init(&pDriverVoid->mCommon.mSpeechQueue)))
  {
    uttserr_log(__FILE__, __LINE__, result,
      "_utts_queue_init failed during boot!",
      "%s%p\n", "pDriverVoid @ ", (void *)pDriverVoid);
    return uttserr_repack(uttserrpart_driver_void, result);
  }

#if !defined(UTTS_SILENT)
  uttserr_log(__FILE__, __LINE__, result,
      "boot ok",
      "%s%p\n", "pDriverVoid @ ", (void *)pDriverVoid);
#endif

  return 0;
}

static int _utts_driver_void_halt(void const *_unused0, utts_driver_t *pDriver)
{
  int result;
  utts_driver_void_t *pDriverVoid;

  (void)_unused0; /* ignore the unused parameter */

  pDriverVoid = _utts_driver_void_cast(pDriver);
  if (NULL == pDriverVoid)
    return uttserr_argpack(
      uttserrpart_driver_void, uttserrcode_bad_argument, 2);

  if (0 != (result = _utts_queue_done(&pDriverVoid->mCommon.mSpeechQueue)))
  {
    uttserr_log(__FILE__, __LINE__, result,
      "_utts_queue_done failed during boot!",
      "%s%p\n", "pDriverVoid @ ", (void *)pDriverVoid);
    return uttserr_repack(uttserrpart_driver_void, result);
  }

  clearmem(
    (char *)pDriverVoid + sizeof(utts_driver_t),
    sizeof(utts_driver_void_t) - sizeof(utts_driver_t));

#if !defined(UTTS_SILENT)
  uttserr_log(__FILE__, __LINE__, result,
      "halt ok",
      "%s%p\n", "pDriverVoid @ ", (void *)pDriverVoid);
#endif

  return result;
}

static int _utts_driver_void_render(
  utts_render_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;
  utts_driver_void_t *pDriverVoid;

  if (NULL == pArgs)
    return uttserr_argpack(
      uttserrpart_driver_void, uttserrcode_null_argument, 1);

  pDriverVoid = _utts_driver_void_cast(pDriver);
  if (NULL == pDriverVoid)
    return uttserr_argpack(
      uttserrpart_driver_void, uttserrcode_bad_argument, 2);

#if !defined(UTTS_SILENT)
  uttserr_log(__FILE__, __LINE__,
    uttserr_pack(uttserrpart_driver_void, 0, 0),
    "render called",
    "%s%p\n", "pDriverVoid @ ", (void *)pDriverVoid);
#endif

  return (result = 0);
}

static int _utts_driver_void_play(
  utts_play_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;
  utts_driver_void_t *pDriverVoid;

  if (NULL == pArgs)
    return uttserr_argpack(
      uttserrpart_driver_void, uttserrcode_null_argument, 1);

  pDriverVoid = _utts_driver_void_cast(pDriver);
  if (NULL == pDriverVoid)
    return uttserr_argpack(
      uttserrpart_driver_void, uttserrcode_null_argument, 2);

#if !defined(UTTS_SILENT)
  uttserr_log(__FILE__, __LINE__,
    uttserr_pack(uttserrpart_driver_void, 0, 0),
    "play called",
    "%s%p\n", "pDriverVoid @ ", (void *)pDriverVoid);
#endif


  return (result = 0);
}

#endif