#ifndef _UTTSDRI_COMMON_INCLUDED_
#define _UTTSDRI_COMMON_INCLUDED_

#include "proto.h"

/* common driver structure (shared by all engine specific drivers) */
typedef struct utts_driver_common
{
  utts_driver_t mUserHead;
  utts_queue_t mSpeechQueue;
} utts_driver_common_t;

/* common driver address cast */
static utts_driver_common_t *_utts_driver_common_cast(utts_driver_t *);

/* common implementation */
static int _utts_driver_common_feedq(utts_feedq_arg_t *, utts_driver_t *);
static int _utts_driver_common_pumpq(utts_pumpq_arg_t *, utts_driver_t *);

#endif
