#ifndef _UTTS_ACTION_TABLE_INCLUDED_
#define _UTTS_ACTION_TABLE_INCLUDED_

#include "../private.h"

/* bounds check driver type enum and check implementation table */
static int _utts_driver_type_bounds_check(utts_driver_type_e);

/* Allocate a particular driver structure and set the driver type flag. */
static int _utts_alloc_driver(
    utts_driver_type_e, unsigned long, utts_driver_t **);

/* Deallocate a driver structure and cross check the driver type against the one
 * stored in the structure. Deallocation may still be performed even if problems
 * are detected, and the address ppDriver refers to will no longer refer to a
 * valid driver structure. */
static int _utts_dealloc_driver(
    utts_driver_type_e, unsigned long, utts_driver_t **);

/* Ensure that the action tables are populated. */
static void _utts_ensure_atab();

/* Uniform driver call layer. */

/* Start a driver either after being allocated or halted. */
static int _utts_driver_boot(utts_driver_t *);

/* Halt a driver after it's started. */
static int _utts_driver_halt(utts_driver_t *);

/* Push multiple commands in to the driver command queue. */
static int _utts_driver_feedq(utts_feedq_arg_t *, utts_driver_t *);

/* Pump the driver command queue. */
static int _utts_driver_pumpq(utts_pumpq_arg_t *, utts_driver_t *);

/* Driver render speech. */
static int _utts_driver_render(utts_render_arg_t *, utts_driver_t *);

/* Driver play speech. */
static int _utts_driver_play(utts_play_arg_t *, utts_driver_t *);

#endif