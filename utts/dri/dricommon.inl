#ifndef _UTTS_COMMON_IMPL_
#define _UTTS_COMMON_IMPL_

#include "dricommon.h"

/* common driver address cast */
static utts_driver_common_t *_utts_driver_common_cast(utts_driver_t *pDriver)
{
  return (utts_driver_common_t *)pDriver;
}

/* common implementation */
static int _utts_driver_common_feedq(
  utts_feedq_arg_t *pArg, utts_driver_t *pDriver)
{
  int result;
  utts_driver_common_t *pDriverCommon;

  pDriverCommon = _utts_driver_common_cast(pDriver);
  if (NULL == pDriverCommon)
    return uttserr_pack(
      uttserrpart_driver_common, uttserrcode_bad_argument, -2);

  if (NULL == pArg)
    return uttserr_pack(
      uttserrpart_driver_common, uttserrcode_null_argument, -1);

  if (NULL == pArg->pSpeechDescArray)
    return uttserr_pack(
      uttserrpart_driver_common, uttserrcode_bad_argument, -1);

  if (0 == pArg->nSpeechDesc)
    return 0; /* trivial success */

  /* currently in this sample implementation there is no way for a queue to get
   * full, but the proper code for a full queue is 1, which will be passed
   * through if it is implemented. */
  while (0 < pArg->nSpeechDesc)
  {
    result = _utts_queue_push(
      pArg->pSpeechDescArray, &pDriverCommon->mSpeechQueue);
    switch (result)
    {
    case 0:
      ++pArg->pSpeechDescArray;
      --pArg->nSpeechDesc;
      break;
    default:
      return result; /* some other fault, such as queue full. */
      break;
    }
  }

  return 0;
}

static int _utts_driver_common_pumpq(
  utts_pumpq_arg_t *pArg, utts_driver_t *pDriver)
{
  int result;
  utts_driver_common_t *pDriverCommon;
  unsigned long n;
  utts_speech_descriptor_t *pS;

  pDriverCommon = _utts_driver_common_cast(pDriver);
  if (NULL == pDriverCommon)
    return uttserr_pack(
      uttserrpart_driver_common, uttserrcode_bad_argument, -2);

  if (NULL == pArg)
    return uttserr_pack(
      uttserrpart_driver_common, uttserrcode_null_argument, -1);

  if (NULL == (pS = pArg->pSpeechDescArray))
    return uttserr_pack(
      uttserrpart_driver_common, uttserrcode_bad_argument, -1);

  result = 0;
  n = pArg->nSpeechDesc;

  while (0 < n)
  {
    if (0 != (result = _utts_queue_pop(pS++, &pDriverCommon->mSpeechQueue)))
      break;
    --n;
  }

  if (uttserr_pack(uttserrpart_queue, uttserrcode_queue_empty, 0) == result)
    result = uttserr_pack(
      uttserrpart_driver_common, uttserrcode_queue_empty, 0);

  pArg->nSpeechDesc -= n;

  return result;
}

#endif