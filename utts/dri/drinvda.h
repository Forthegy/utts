#ifndef _UTTS_DRIVER_NVDA_INCLUDED_
#define _UTTS_DRIVER_NVDA_INCLUDED_

#include "proto.h"

#if defined(UTTS_WINDOWS)

/* all drivers depend on at least some of the common driver components */
#include "dricommon.h"

#if defined(_WIN64)
#define _UTTS_NVDA_CLIENT_DLL "nvdaControllerClient64.dll"
#else
#define _UTTS_NVDA_CLIENT_DLL "nvdaControllerClient32.dll"
#endif

/* NVDA controller call signatures */
typedef error_status_t (__stdcall *pfnnvda_speakText_t)(const wchar_t *);
/* typedef error_status_t (__stdcall *pfnnvda_brailleMessage_t)(const wchar_t *);*/
typedef error_status_t (__stdcall *pfnnvda_cancelSpeech_t)();
typedef error_status_t (__stdcall *pfnnvda_testIfRunning_t)();

/* NVDA controller driver structure */
typedef struct utts_driver_nvda
{
  utts_driver_common_t mCommon;

  HMODULE nvdaLibrary;
  pfnnvda_speakText_t speakText;
  pfnnvda_cancelSpeech_t cancelSpeech;
  pfnnvda_testIfRunning_t  testIfRunning;
} utts_driver_nvda_t;

/* nvda driver address cast */
static utts_driver_nvda_t *_utts_driver_nvda_cast(utts_driver_t *);

/* nvda driver implementation */
static int _utts_driver_nvda_boot(void const * /* unused */, utts_driver_t *);
static int _utts_driver_nvda_halt(void const * /* unused */, utts_driver_t *);

static int _utts_driver_nvda_render(utts_render_arg_t *, utts_driver_t *);
static int _utts_driver_nvda_play(utts_play_arg_t *, utts_driver_t *);

#endif

#endif