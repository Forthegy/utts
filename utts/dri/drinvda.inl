#ifndef _UTTS_DRIVER_NVDA_IMPL_
#define _UTTS_DRIVER_NVDA_IMPL_

#include "drinvda.h"

#if defined(UTTS_WINDOWS)

/* nvda driver address cast */
static utts_driver_nvda_t *_utts_driver_nvda_cast(utts_driver_t *pDriver)
{
  if (NULL != pDriver && utts_driver_nvda_type == pDriver->eType)
    return (utts_driver_nvda_t *)pDriver;
  else
    return NULL;
}

/* nvda driver implementation */
static int _utts_driver_nvda_boot(void const *_unused0, utts_driver_t *pDriver)
{
  int result;
  utts_driver_nvda_t *pDriverNVDA;
    
  (void)_unused0; /* ignore the unused parameter */

  pDriverNVDA = _utts_driver_nvda_cast(pDriver);
  if (NULL == pDriverNVDA)
    return -1; /* first argument error */

  if (0 != (result = _utts_queue_init(&pDriverNVDA->mCommon.mSpeechQueue)))
  {
#if !defined(UTTS_NOSTDIO)
    fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> failed to boot!\n");
    fprintf(stderr, "%s%d%s", "  reason : _utts_queue_init failed with code", result, ".\n");
#endif
    return 6;
  }

  if (NULL == (pDriverNVDA->nvdaLibrary = LoadLibrary(_UTTS_NVDA_CLIENT_DLL)))
  {
    (void)_utts_queue_done(&pDriverNVDA->mCommon.mSpeechQueue);
#if !defined(UTTS_NOSTDIO)
    fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> failed to boot!\n");
    fprintf(stderr, "%s",     "  reason : LoadLibrary failed to open \"" _UTTS_NVDA_CLIENT_DLL "\".\n");
#endif
    return 1125;
  }

  if (NULL == (pDriverNVDA->speakText = (pfnnvda_speakText_t)GetProcAddress(pDriverNVDA->nvdaLibrary, "nvdaController_speakText")))
  {
    (void)_utts_queue_done(&pDriverNVDA->mCommon.mSpeechQueue);
    (void)FreeLibrary(pDriverNVDA->nvdaLibrary);
    pDriverNVDA->nvdaLibrary = NULL;
#if !defined(UTTS_NOSTDIO)
    fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> failed to boot!\n");
    fprintf(stderr, "%s",     "  reason : GetProcAddress failed to find \"nvdaController_speakText\" in \"" _UTTS_NVDA_CLIENT_DLL "\".\n");
#endif
    return 1126;
  }

  if (NULL == (pDriverNVDA->cancelSpeech = (pfnnvda_cancelSpeech_t)GetProcAddress(pDriverNVDA->nvdaLibrary, "nvdaController_cancelSpeech")))
  {
    (void)_utts_queue_done(&pDriverNVDA->mCommon.mSpeechQueue);
    (void)FreeLibrary(pDriverNVDA->nvdaLibrary);
    pDriverNVDA->nvdaLibrary = NULL;
#if !defined(UTTS_NOSTDIO)
    fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> failed to boot!\n");
    fprintf(stderr, "%s",     "  reason : GetProcAddress failed to find \"nvdaController_cancelSpeech\" in \"" _UTTS_NVDA_CLIENT_DLL "\".\n");
#endif
    return 1127;
  }

  if (NULL == (pDriverNVDA->testIfRunning = (pfnnvda_testIfRunning_t)GetProcAddress(pDriverNVDA->nvdaLibrary, "nvdaController_testIfRunning")))
  {
    (void)_utts_queue_done(&pDriverNVDA->mCommon.mSpeechQueue);
    (void)FreeLibrary(pDriverNVDA->nvdaLibrary);
    pDriverNVDA->nvdaLibrary = NULL;
#if !defined(UTTS_NOSTDIO)
    fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> failed to boot!\n");
    fprintf(stderr, "%s",     "  reason : GetProcAddress failed to find \"nvdaController_testIfRunning\" in \"" _UTTS_NVDA_CLIENT_DLL "\".\n");
#endif
    return 1128;
  }

#if !defined(UTTS_NOSTDIO)
  fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> booted.\n");
#endif

  return 0;
}

static int _utts_driver_nvda_halt(void const *_unused0, utts_driver_t *pDriver)
{
  int result;
  utts_driver_nvda_t *pDriverNVDA;

  (void)_unused0; /* ignore the unused parameter */

  pDriverNVDA = _utts_driver_nvda_cast(pDriver);
  if (NULL == pDriverNVDA)
    return -1; /* first argument error */

  if (0 != (result = _utts_queue_done(&pDriverNVDA->mCommon.mSpeechQueue)))
  {
#if !defined(UTTS_NOSTDIO)
    fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> error during halt!\n");
    fprintf(stderr, "%s%d%s", "  reason : _utts_queue_done failed with code", result, ".\n");
#endif
    result = 7;
  }

  if (NULL != pDriverNVDA->cancelSpeech)
    (void)pDriverNVDA->cancelSpeech();

  if (NULL != pDriverNVDA->nvdaLibrary)
  {
    (void)FreeLibrary(pDriverNVDA->nvdaLibrary);
    pDriverNVDA->nvdaLibrary = NULL;
  }

  clearmem((void *)pDriverNVDA, sizeof(utts_driver_nvda_t));

#if !defined(UTTS_NOSTDIO)
  fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> halted.\n");
#endif

  return result;
}

static int _utts_driver_nvda_render(utts_render_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;
  utts_driver_nvda_t *pDriverNVDA;

  if (NULL == pArgs)
    return uttserr_argpack(uttserrpart_driver_nvda, uttserrcode_null_argument, 1);

  pDriverNVDA = _utts_driver_nvda_cast(pDriver);
  if (NULL == pDriverNVDA)
    return uttserr_argpack(uttserrpart_driver_nvda, uttserrcode_null_argument, 2);

#if !defined(UTTS_NOSTDIO)
  fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> render called.\n");
#endif

  return (result = 0);
}

static int _utts_driver_nvda_play(utts_play_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;
  utts_driver_nvda_t *pDriverNVDA;

  if (NULL == pArgs)
    return uttserr_argpack(uttserrpart_driver_nvda, uttserrcode_null_argument, 1);

  pDriverNVDA = _utts_driver_nvda_cast(pDriver);
  if (NULL == pDriverNVDA)
    return uttserr_argpack(uttserrpart_driver_nvda, uttserrcode_null_argument, 2);

#if !defined(UTTS_NOSTDIO)
  fprintf(stderr, "%s%p%s", "utts NVDA driver <", (void *)pDriverNVDA, "> play called.\n");
#endif

  return (result = 0);
}

#endif

#endif