#ifndef _UTTS_DRIVER_SAPI_IMPL_
#define _UTTS_DRIVER_SAPI_IMPL_

#include "drisapi.h"

/* TODO: update my return codes. */

#if defined(UTTS_WINDOWS)

static utts_driver_sapi_t *_utts_driver_sapi_cast(utts_driver_t *pDriver)
{
  if (NULL != pDriver && utts_driver_sapi_type == pDriver->eType)
    return (utts_driver_sapi_t *)pDriver;
  else
    return NULL;
}

/* sapi driver implementation */
static int _utts_driver_sapi_boot(void const *_unused0, utts_driver_t *pDriver)
{
  int result;
  utts_driver_sapi_t *pDriverSAPI;
    
  (void)_unused0; /* ignore the unused parameter */

  pDriverSAPI = _utts_driver_sapi_cast(pDriver);
  if (NULL == pDriverSAPI)
    return -1; /* first argument error */

  if (0 != (result = _utts_queue_init(&pDriverSAPI->mCommon.mSpeechQueue)))
  {
#if !defined(UTTS_NOSTDIO)
    fprintf(stderr, "%s%p%s", "utts SAPI driver <", (void *)pDriverSAPI, "> failed to boot!\n");
    fprintf(stderr, "%s%d%s", "  reason : _utts_queue_init failed with code", result, ".\n");
#endif
    return 6;
  }

#if !defined(UTTS_NOSTDIO)
  fprintf(stderr, "%s%p%s", "utts SAPI driver <", (void *)pDriverSAPI, "> booted.\n");
#endif

  return (result = 0);
}

static int _utts_driver_sapi_halt(void const *_unused0, utts_driver_t *pDriver)
{
  int result;
  utts_driver_sapi_t *pDriverSAPI;

  (void)_unused0; /* ignore the unused parameter */

  pDriverSAPI = _utts_driver_sapi_cast(pDriver);
  if (NULL == pDriverSAPI)
    return -1; /* first argument error */

  if (0 != (result = _utts_queue_done(&pDriverSAPI->mCommon.mSpeechQueue)))
  {
#if !defined(UTTS_NOSTDIO)
    fprintf(stderr, "%s%p%s", "utts SAPI driver <", (void *)pDriverSAPI, "> error during halt!\n");
    fprintf(stderr, "%s%d%s", "  reason : _utts_queue_done failed with code", result, ".\n");
#endif
    result = 7;
  }

  clearmem((void *)pDriverSAPI, sizeof(utts_driver_sapi_t));

#if !defined(UTTS_NOSTDIO)
  fprintf(stderr, "%s%p%s", "utts SAPI driver <", (void *)pDriverSAPI, "> halted.\n");
#endif

  return (result = 0);
}

static int _utts_driver_sapi_render(utts_render_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;
  utts_driver_sapi_t *pDriverSAPI;

  if (NULL == pArgs)
    return uttserr_argpack(uttserrpart_driver_sapi, uttserrcode_null_argument, 1);

  pDriverSAPI = _utts_driver_sapi_cast(pDriver);
  if (NULL == pDriverSAPI)
    return uttserr_argpack(uttserrpart_driver_sapi, uttserrcode_null_argument, 2);

#if !defined(UTTS_NOSTDIO)
  fprintf(stderr, "%s%p%s", "utts SAPI driver <", (void *)pDriverSAPI, "> render called.\n");
#endif

  return (result = 0);
}

static int _utts_driver_sapi_play(utts_play_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;
  utts_driver_sapi_t *pDriverSAPI;

  if (NULL == pArgs)
    return uttserr_argpack(uttserrpart_driver_sapi, uttserrcode_null_argument, 1);

  pDriverSAPI = _utts_driver_sapi_cast(pDriver);
  if (NULL == pDriverSAPI)
    return uttserr_argpack(uttserrpart_driver_sapi, uttserrcode_null_argument, 2);

#if !defined(UTTS_NOSTDIO)
  fprintf(stderr, "%s%p%s", "utts SAPI driver <", (void *)pDriverSAPI, "> play called.\n");
#endif

  return (result = 0);
}

#endif

#endif
