#ifndef _UTTS_ACTION_TABLE_IMPL_
#define _UTTS_ACTION_TABLE_IMPL_

#include "atab.h"

#include "dricommon.h"
#include "drivoid.h"
#include "drisapi.h"
#include "drinvda.h"

/* Driver name and enum. */
char const *utts_driver_type_name(utts_driver_type_e eType)
{
  switch (eType)
  {
  case utts_driver_void_type:
    return "VOID";
    break;
  case utts_driver_sapi_type:
    return "SAPI";
    break;
  case utts_driver_nvda_type:
    return "NVDA";
    break;
  case utts_driver_espeak_type:
    return "ESPEAK";
    break;
  case utts_driver_user_type:
    return "USER";
    break;
  }

  return NULL;
}

char const *utts_driver_name(utts_driver_t *pDriver)
{
  if (NULL == pDriver)
    return NULL;

  return utts_driver_type_name(pDriver->eType);
}

utts_driver_type_e utts_driver_name_type(char const *pDriverName)
{
  if (0 == strncmp(pDriverName, "VOID", 4))
    return utts_driver_void_type;
  else if (0 == strncmp(pDriverName, "SAPI", 4))
    return utts_driver_sapi_type;
  else if (0 == strncmp(pDriverName, "NVDA", 4))
    return utts_driver_nvda_type;
  else if (0 == strncmp(pDriverName, "ESPEAK", 6))
    return utts_driver_espeak_type;
  else if (0 == strncmp(pDriverName, "USER", 4))
    return utts_driver_user_type;
  else
    return utts_driver_none;
}

/* Action table initialization state. */
static int _utts_atab_init = 0; /* set to magic number on init */
static int _utts_implemented_btab = 0; /* bitfield indicating implementation status of drivers */

/* Action tables. */
static pfn_utts_driver_boot_t _utts_boot_atab[utts_driver_type_count] = { 0 };
static pfn_utts_driver_halt_t _utts_halt_atab[utts_driver_type_count] = { 0 };
static pfn_utts_driver_feedq_t _utts_feedq_atab[utts_driver_type_count] = { 0 };
static pfn_utts_driver_pumpq_t _utts_pumpq_atab[utts_driver_type_count] = { 0 };
static pfn_utts_driver_render_t _utts_render_atab[utts_driver_type_count] = {0};
static pfn_utts_driver_play_t _utts_play_atab[utts_driver_type_count] = { 0 };

/* bounds check driver type enum and check implementation table */
static int _utts_driver_type_bounds_check(utts_driver_type_e eType)
{
  (void)_utts_ensure_atab();

  if (eType < 0 || eType >= utts_driver_type_count)
    return uttserr_pack(
      uttserrpart_core_table, uttserrcode_invalid_driver, eType);

  if (_utts_implemented_btab & (1 << eType))
    return 0;

  return uttserr_pack(
    uttserrpart_core_table, uttserrcode_noimpl_driver, eType);
}

/* Allocate a particular driver structure and set the driver type flag. */
static int _utts_alloc_driver(
  utts_driver_type_e driverType, unsigned long szhint,
  utts_driver_t **ppDriver)
{
  int result;
  unsigned long sz;

  if (NULL == ppDriver)
    return uttserr_argpack(
      uttserrpart_core_alloc, uttserrcode_null_argument, 1);

  if (0 != (result = _utts_driver_type_bounds_check(driverType)))
    return result; /* driver type bounds check failed */

  switch (driverType)
  {
  case utts_driver_void_type:
    sz = sizeof(utts_driver_void_t);
    break;
  case utts_driver_sapi_type:
    sz = sizeof(utts_driver_sapi_t);
    break;
  case utts_driver_nvda_type:
    sz = sizeof(utts_driver_nvda_t);
    break;
  default:
    return uttserr_argpack(
      uttserrpart_core_alloc, uttserrcode_bad_argument, 2);
    /* fall through */
  }

  *ppDriver = (utts_driver_t *)malloc(sz);
  if (NULL == *ppDriver)
    return uttserr_pack(
      uttserrpart_core_alloc, uttserrcode_out_of_memory, 0);

  clearmem(*ppDriver, sz);         /* zero the memory */
  (*ppDriver)->eType = driverType; /* set the driver type tag */

  return 0;
}

/* Deallocate a driver structure and cross check the driver type against the one
 * stored in the structure. Deallocation may still be performed even if problems
 * are detected, and the address ppDriver refers to will no longer refer to a
 * valid driver structure. */
static int _utts_dealloc_driver(
  utts_driver_type_e driverType, unsigned long szhint,
  utts_driver_t **ppDriver)
{
  int result;

  if (NULL == ppDriver)
    return uttserr_argpack(
      uttserrpart_core_alloc, uttserrcode_null_argument, 3);
  
  if (NULL == *ppDriver)
    return uttserr_pack(
      uttserrpart_core_alloc, uttserrcode_dealloc_null, 0);

  result = 0;
  if (driverType != (*ppDriver)->eType)
  {
    result = uttserr_pack(uttserrpart_core_alloc,
      uttserrcode_dealloc_driver_mismatch,
      (*ppDriver)->eType);

    uttserr_log(__FILE__, __LINE__,
      result,
      "Driver type mismatch on dealloc!",
      "%s%p" "%s%s%s" "%s%s",
      "deallocating driver <", *ppDriver,
      ":", utts_driver_name(*ppDriver), "> but caller reports type as ",
      utts_driver_type_name(driverType), "\n");
  }

  free((void *)*ppDriver);
  *ppDriver = NULL;
  return result;
}

/* Ensure that the action tables are populated. */
static void _utts_ensure_atab()
{
  /* a number that is extremely unlikely to appear randomly */
# define _MAGIC 0xA55AC33C
  if (_MAGIC != _utts_atab_init)
  {
    _utts_atab_init = 0;        /* Clear initialized indicator bits. */
    _utts_implemented_btab = 0; /* Clear implementation indicator bits. */

    /* The void type must always be implemented. */
    _utts_implemented_btab |= 1 << utts_driver_void_type;

    /* The following drivers can always be implemented on Windows. */
#if defined(UTTS_WINDOWS)
    /* The Microsoft SAPI driver implementation uses the daemon configuration
     * and dispatches commands asynchronously to perform text-to-speech. */
    _utts_implemented_btab |= 1 << utts_driver_sapi_type;

    /* The NVDA driver implementation uses the nvdaController32.dll (resp. 64)
     * ABI to perform live text to speech without any additional threads in the
     * current program in 32 (resp. 64) bit Windows. */
    _utts_implemented_btab |= 1 << utts_driver_nvda_type;
#endif
    /* Drivers not yet implemented. */
    /* _utts_implemented_btab |= 1 << utts_driver_espeak_type; */
    /* _utts_implemented_btab |= 1 << utts_driver_user_type; */

    /* Zero memory in each action table. */
    clearmem((void *)&_utts_boot_atab[0], sizeof(_utts_boot_atab));
    clearmem((void *)&_utts_halt_atab[0], sizeof(_utts_halt_atab));
    clearmem((void *)&_utts_feedq_atab[0], sizeof(_utts_feedq_atab));
    clearmem((void *)&_utts_pumpq_atab[0], sizeof(_utts_pumpq_atab));
    clearmem((void *)&_utts_render_atab[0], sizeof(_utts_render_atab));
    clearmem((void *)&_utts_play_atab[0], sizeof(_utts_play_atab));

    /* boot call mapping */
    _utts_boot_atab[utts_driver_void_type] = &_utts_driver_void_boot;
#if defined(UTTS_WINDOWS)
    _utts_boot_atab[utts_driver_sapi_type] = &_utts_driver_sapi_boot;
    _utts_boot_atab[utts_driver_nvda_type] = &_utts_driver_nvda_boot;
#else
    _utts_boot_atab[utts_driver_sapi_type] = NULL;
    _utts_boot_atab[utts_driver_nvda_type] = NULL;
#endif
    _utts_boot_atab[utts_driver_espeak_type] = NULL; /* not implemented */
    _utts_boot_atab[utts_driver_user_type] = NULL; /* not implemented */

    /* halt call mapping */
    _utts_halt_atab[utts_driver_void_type] = &_utts_driver_void_halt;
#if defined(UTTS_WINDOWS)
    _utts_halt_atab[utts_driver_sapi_type] = &_utts_driver_sapi_halt;
    _utts_halt_atab[utts_driver_nvda_type] = &_utts_driver_nvda_halt;
#else
    _utts_halt_atab[utts_driver_sapi_type] = NULL;
    _utts_halt_atab[utts_driver_nvda_type] = NULL;
#endif
    _utts_halt_atab[utts_driver_espeak_type] = NULL; /* not implemented */
    _utts_halt_atab[utts_driver_user_type] = NULL; /* not implemented */

    /* feedq call mapping (common interface component) */
    _utts_feedq_atab[utts_driver_void_type] = &_utts_driver_common_feedq;
    _utts_feedq_atab[utts_driver_sapi_type] = &_utts_driver_common_feedq;
    _utts_feedq_atab[utts_driver_nvda_type] = &_utts_driver_common_feedq;
    _utts_feedq_atab[utts_driver_espeak_type] = &_utts_driver_common_feedq;
    _utts_feedq_atab[utts_driver_user_type] = &_utts_driver_common_feedq;

    /* pumpq call mapping (common daemon component) */
    _utts_pumpq_atab[utts_driver_void_type] = &_utts_driver_common_pumpq;
    _utts_pumpq_atab[utts_driver_sapi_type] = &_utts_driver_common_pumpq;
    _utts_pumpq_atab[utts_driver_nvda_type] = &_utts_driver_common_pumpq;
    _utts_pumpq_atab[utts_driver_espeak_type] = &_utts_driver_common_pumpq;
    _utts_pumpq_atab[utts_driver_user_type] = &_utts_driver_common_pumpq;

    /* render call mapping */
    _utts_render_atab[utts_driver_void_type] = &_utts_driver_void_render;
#if defined(UTTS_WINDOWS)
    _utts_render_atab[utts_driver_sapi_type] = &_utts_driver_sapi_render;
    _utts_render_atab[utts_driver_nvda_type] = &_utts_driver_nvda_render;
#else
    _utts_render_atab[utts_driver_sapi_type] = NULL;
    _utts_render_atab[utts_driver_nvda_type] = NULL;
#endif
    _utts_render_atab[utts_driver_espeak_type] = NULL; /* not implemented */
    _utts_render_atab[utts_driver_user_type] = NULL; /* not implemented */

    /* play call mapping */
    _utts_play_atab[utts_driver_void_type] = &_utts_driver_void_play;
#if defined(UTTS_WINDOWS)
    _utts_play_atab[utts_driver_sapi_type] = &_utts_driver_sapi_play;
    _utts_play_atab[utts_driver_nvda_type] = &_utts_driver_nvda_play;
#else
    _utts_play_atab[utts_driver_sapi_type] = NULL;
    _utts_play_atab[utts_driver_nvda_type] = NULL;
#endif
    _utts_play_atab[utts_driver_espeak_type] = NULL; /* not implemented */
    _utts_play_atab[utts_driver_user_type] = NULL; /* not implemented */

    /* Finished initializing action tables, set initialized indicator to magic
     * bits. */
    _utts_atab_init = _MAGIC;
  }
#  undef _MAGIC
}

/* Driver call implementation. This is implemented uniformly and indifferently
 * to each driver, and is where uniformity of return code conventions should be
 * enforced. The form of all driver calls is to accept two arguments, the first
 * of which is an address whose interpretation is subject to the driver, and the
 * second of which references the location of the driver struct, and the return
 * value is always an int that is 0 on success or uttserr packed structure on
 * failure. */
#define _UTTS_DRICALL(CallName, pArg, pDriver, pReturn)             \
{                                                                   \
  int *_UDpResult;                                                  \
  utts_driver_t *_UDpDri;                                           \
  pfn_utts_driver_##CallName##_t _UDpfn;                            \
  utts_driver_type_e _UDtype;                                       \
                                                                    \
  _UDpResult = (pReturn);                                           \
  _UDpDri = (pDriver);                                              \
  _UDtype = _UDpDri->eType;                                         \
  if (0 == (*_UDpResult = _utts_driver_type_bounds_check(_UDtype))) \
  {                                                                 \
    _UDpfn = _utts_##CallName##_atab[_UDtype];                      \
    if (NULL == _UDpfn)                                             \
      /* missing driver call */                                     \
      *_UDpResult = uttserr_pack(                                   \
        uttserrpart_core_ifc, uttserrcode_missing_driver_proc, 0);  \
    else                                                            \
      *_UDpResult = _UDpfn((pArg), _UDpDri);                        \
  }                                                                 \
  if (0 != *_UDpResult)                                             \
  {                                                                 \
    uttserr_log(__FILE__, __LINE__,                                 \
      *_UDpResult,                                                  \
      "_UTTS_DRICALL " #CallName " non-zero code!",                 \
      "%s%p" "%s%s" "%s%p" "%s",                                    \
      "driver <", _UDpDri,                                          \
      ":", utts_driver_name(_UDpDri),                               \
      "+" #CallName , (pArg),                                       \
      ">\n");                                                       \
  }                                                                 \
}

#define _UTTS_DRIARG(Name, No) \
  if (NULL == (Name))          \
    return uttserr_pack(       \
      uttserrpart_core_ifc, uttserrcode_null_argument, (No));

/* Uniform driver call layer. */

static int _utts_driver_boot(utts_driver_t *pDriver)
{
  int result;

  _UTTS_DRIARG(pDriver, -1);
  _UTTS_DRICALL(boot, NULL, pDriver, &result);

  return result;
}

static int _utts_driver_halt(utts_driver_t *pDriver)
{
  int result;

  _UTTS_DRIARG(pDriver, -1);
  _UTTS_DRICALL(halt, NULL, pDriver, &result);

  return result;
}

static int _utts_driver_feedq(utts_feedq_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;

  _UTTS_DRIARG(pDriver, -2);
  _UTTS_DRIARG(pArgs, -1);
  _UTTS_DRICALL(feedq, pArgs, pDriver, &result);

  return result;
}

static int _utts_driver_pumpq(utts_pumpq_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;

  _UTTS_DRIARG(pDriver, -2);
  _UTTS_DRIARG(pArgs, -1);
  _UTTS_DRICALL(pumpq, pArgs, pDriver, &result);

  return result;
}

static int _utts_driver_render(utts_render_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;

  _UTTS_DRIARG(pDriver, -2);
  _UTTS_DRIARG(pArgs, -1);
  _UTTS_DRICALL(render, pArgs, pDriver, &result);

  return result;
}

static int _utts_driver_play(utts_play_arg_t *pArgs, utts_driver_t *pDriver)
{
  int result;

  _UTTS_DRIARG(pDriver, -2);
  _UTTS_DRIARG(pArgs, -1);
  _UTTS_DRICALL(play, pArgs, pDriver, &result);

  return result;
}

#endif