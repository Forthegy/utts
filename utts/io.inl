#ifndef _UTTS_IO_IMPL_
#define _UTTS_IO_IMPL_

#include "utts.h"

#if defined(UTTS_HAVE_IO_API)
#if !defined(UTTS_NOSTDIO)

#include <stdarg.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>

int utts_vioprintf(struct utts_iospec const *pIO,
  char const *pFormat, va_list va)
{
  int result;

  if (NULL == pIO)
    return uttserr_argpack(uttserrpart_io, uttserrcode_null_argument, 1);

  if (NULL == pIO->pData)
    return uttserr_argpack(uttserrpart_io, uttserrcode_bad_argument, 1);

  switch (pIO->kind)
  {
  case uttsio_stdiofile:
    /* file */
    result = vfprintf((FILE *)pIO->pData, pFormat, va);
    break;
  case uttsio_stdiobuf:
    /* buffer */
    result = vsnprintf((char *)pIO->pData, pIO->sz, pFormat, va);
    break;
  default:
    result = uttserr_argpack(uttserrpart_io, uttserrcode_bad_argument, 1);
    break;
  };

  return result;
}

int utts_ioprintf(struct utts_iospec const *pIO, char const *pFormat, ...)
{
  int result;
  va_list va;

#if !defined(UTTS_NOSTDARG)
  va_start(va, pFormat);
  result = utts_vioprintf(pIO, pFormat, va);
  va_end(va);
#else
  result = uttserr_pack(uttserrpart_io, uttserrpart_nocarg, 0);
#endif

  return result;
}

#else
int utts_vioprintf(struct utts_iospec const *pIO,
  char const *pFormat, va_list va)
{
  return 0;
}

int utts_ioprintf(struct utts_iospec const *pIO, char const *pFormat, ...)
{
#if !defined(UTTS_NOSTDARG)
  return 0;
#else
  return uttserr_pack(uttserrpart_io, uttserrpart_nocarg, 0);
#endif
}
#endif
#endif

#endif