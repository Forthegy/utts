/* utts.h
 *
 * An updated backend for the speak library, originally written by Roger
 * Merryfield. This code is released under the unlicence.
 * 
 * */

#ifndef _UTTS_INCLUDED_
#define _UTTS_INCLUDED_

#include <stddef.h>

#if defined(UTTS_NOSTDTIME)
typedef struct utts_timespec
{
  unsigned long long tv_sec;  /* seconds */
  long               tv_nsec; /* nanoseconds */
} utts_timespec_t;
#else
#include <time.h>
typedef struct timespec utts_timespec_t;
#endif

#if defined(UTTS_NOSTDARG)
typedef char *va_list; /* address va_start should give for the argument list */
#else
#include <stdarg.h>
#endif

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define UTTS_WINDOWS
#endif

/* Error coding macro implementation details. */
#include "utts_error.h"

/* Part ID error field.
 *   6 bits -> 63 possibilities. */
typedef enum uttserrpart
{
  /* Zero indicates an unspecified part. */
  uttserrpart_unspecified    = 0,

  /* Core parts. 1-8 (8 possible) */
  uttserrpart_core_table     = 1,
  uttserrpart_core_ifc       = 2,
  uttserrpart_core_alloc     = 3,
  uttserrpart_core_dispatch  = 4,

  /* Daemon parts. 9-16 (8 possible) */
  uttserrpart_daemon_control = 9,
  uttserrpart_daemon_relay   = 10,
  uttserrpart_daemon_cache   = 11,
  uttserrpart_daemon_memory  = 12,

  /* Infrastructural parts. 17-42 (26 possible) */
  uttserrpart_enum           = 17,
  uttserrpart_time           = 18,
  uttserrpart_io             = 19,
  uttserrpart_reserved3      = 20, /* reserved space for system interfaces */
  uttserrpart_reserved4      = 21, /* reserved space for system interfaces */
  uttserrpart_reserved5      = 22, /* reserved space for system interfaces */
  uttserrpart_hashtable      = 23,
  uttserrpart_queue          = 24,
  uttserrpart_thread         = 25,

  /* Driver implementation. 43-63 (21 possible) */
  uttserrpart_driver_common  = 43,
  uttserrpart_driver_void    = 44,
  uttserrpart_driver_sapi    = 45,
  uttserrpart_driver_nvda    = 46,
  uttserrpart_driver_espeak  = 47,
  uttserrpart_driver_user    = 48
} uttserrpart_e;

/* Error codes.
 *   17 bits -> 131,071 possibilitites per part ID
 *   (8,257,473 part/code combinations). */
typedef enum uttserrcode
{
  uttserrcode_none = 0,

  /* Argument errors. */
  uttserrcode_null_argument           = 0x410000,
  uttserrcode_bad_argument            = 0x411000, 

  /* Driver table errors. */
  uttserrcode_invalid_driver          = 0x412000, /* Driver requested is
                                                   * unknown. */
  uttserrcode_noimpl_driver           = 0x413000, /* The requested driver is
                                                   * not implemented. */
  uttserrcode_unavailable_driver      = 0x414000, /* The requested driver is
                                                   * not available. */
  uttserrcode_missing_driver_proc     = 0x415000, /* Driver proc table entry
                                                   * for call is NULL. */

  /* Implementation. */
  uttserrcode_notime                  = 0x421000, /* No timestamp source. */
  uttserrcode_nostdio                 = 0x422000, /* No stdio is available. */
  uttserrcode_nocarg                  = 0x423000, /* No variadic argument
                                                   * processing available. */

  /* C standard library. */
  uttserrcode_strftime                = 0x701000, /* strftime error. */
  uttserrcode_xprintf                 = 0x702000, /* printf encoding errors */

  /* memory errors */
  uttserrcode_out_of_memory           = 0x7F1000,
  uttserrcode_dealloc_null            = 0x7F2000, /* Can't deallocate NULL! */
  uttserrcode_dealloc_driver_mismatch = 0x7F3000, /* Wrong driver indicated in  
                                                   * dealloc parameters. */
  /* Reflection info. */
  uttserrcode_info                    = _uttserr_codebmask,

  /* Each infrastructural component at the moment is being given 32 codes, so
   * that each code uniquely identifies the component whence it originates. */

  /* queue errors */
  uttserrcode_queue_invalid           =  1, /* Queue structure is invalid. */
  uttserrcode_queue_broken            =  2, /* Queue structure is broken. */
  uttserrcode_queue_empty             =  3, /* Queue empty (nothing waiting)*/
  uttserrcode_queue_full              =  4, /* Queue full (can't insert) */
  uttserrcode_queue_push_fault        =  5, /* push implementation fault. */
  uttserrcode_queue_pop_fault         =  6, /* pop implementation fault. */
  uttserrcode_queue_init_fault        =  7, /* init implementation fault. */
  uttserrcode_queue_done_fault        =  8, /* done implementation fault. */

  /* hashtable errors */
  uttserrcode_hashtable_invalid       = 33, /* Hashtable structure is invalid.
                                             * */
  uttserrcode_hashtable_broken        = 34, /* Hashtable structure is broken. */
  uttserrcode_hashtable_invalidkey    = 35, /* Invalid key passed. */
  uttserrcode_hashtable_keymissing    = 36, /* Key not in table. */
  uttserrcode_hashtable_nooverwrite   = 37, /* Something is already at the key
                                             * where an insert was attempted. */
  uttserrcode_hashtable_needrehash    = 38, /* Table is saturated. */
  uttserrcode_hashtable_insert_fault  = 39, /* insert implementation fault. */
  uttserrcode_hashtable_delete_fault  = 40, /* delete implementation fault. */
  uttserrcode_hashtable_rehash_fault  = 41, /* rehash implementation fault. */
  uttserrcode_hashtable_init_fault    = 42, /* init implementation fault. */
  uttserrcode_hashtable_done_fault    = 43  /* done implementation fault. */
} uttserrcode_e;

/* Special user field values whenever the code field of an error is set to
 * uttserrcode_info. */
typedef enum uttserrinfo
{
  uttserrinfo_stub       =  0, /* An API with no implementation was called. */
  uttserrinfo_deprecated = -1, /* An API that was deprecated in the most
                                * recent major version roll was called but the
                                * call succeeded. The need to ignore this in
                                * error checking leads to code that is self
                                * documenting with respect to where calls to
                                * outmoded interfaces still lurk. */
  uttserrinfo_arguments  = -2, /* A warning has been logged about argument
                                * sanity check failures. If the apparently
                                * insane arguments are intentional, this can
                                * be ignored in the user's error checking. */
} uttserrinfo_e;

/* Error encoding bitfields. */
enum
{
  uttserr_usermask  = _uttserr_usermask,  /* user information mask - 0th through
                                           * 8th bits set */
  uttserr_usershft  = _uttserr_usershft,  /* shift for the user information */
  uttserr_userbits  = _uttserr_userbits,  /* significant bits for the user
                                           * information */
  uttserr_userbmask = _uttserr_userbmask, /* base mask (usermask >> usershft) */
  uttserr_usersgnd  = 1,                  /* user field is signed */

  uttserr_partmask  = _uttserr_partmask,  /* part id mask - 9th through 14th
                                           * bits set */
  uttserr_partshft  = _uttserr_partshft,  /* shift for the part id */
  uttserr_partbits  = _uttserr_partbits,  /* significant bits for the part id */
  uttserr_partbmask = _uttserr_partbmask, /* base mask (partmask >> partshft) */
  uttserr_partsgnd  = 0,                  /* part ID field is unsigned */

  uttserr_codemask  = _uttserr_codemask,  /* code mask - 15th through 31st bits
                                           * set */
  uttserr_codeshft  = _uttserr_codeshft,  /* shift for the error code */
  uttserr_codebits  = _uttserr_codebits,  /* significant bits for the code */
  uttserr_codebmask = _uttserr_codebmask, /* base mask (codemask >> codeshft) */
  uttserr_codesgnd  = 1                   /* code field is signed */
};

/* Create an argument error from the given argument index. This consists of an
 * error whose part ID and code fields are 0 and whose user field is -(Index).
 * This can also be passed as the user part of another error to supply context.
 * The allowable indices are 1 through 63. If Index evaluates to a value that is
 * out of range, then the resulting error will not be interpreted as an argument
 * error so, for example, utts_isargerr(uttserr_argerr(64)) == 0 but
 * utts_isargerr(uttserr_argerr(2)) == 1. */
#define uttserr_argerr(Index) _uttserr_argerr((Index))

/* Returns one if and only if Err evaluates to an error whose user field
 * represents an argument, otherwise 0. */
#define uttserr_isargerr(Err) _uttserr_isargerr((Err))

/* Get the argument number (1 through 63 inclusive) of an argument error, or 0
 * if the given error is not an argument error. */
#define uttserr_getarg(Err) _uttserr_getarg((Err))

/* Get the user field of an error (signed 9 bit integer). */
#define uttserr_getuser(Err) _uttserr_getuser((Err))

/* Get the part ID field of an error (unsigned 6 bit integer). */
#define uttserr_getpart(Err) _uttserr_getpart((Err))

/* Get the code field of an error (signed 17 bit integer). */
#define uttserr_getcode(Err) _uttserr_getcode((Err))

/* Pack an error from the part ID, code, and user fields. */
#define uttserr_pack(Part, Code, User) \
  _uttserr_packerr((Part), (Code), (User))

/* Pack an argument error from the part ID and code fields and the argument
 * index between 1 and 63. */
#define uttserr_argpack(Part, Code, Index) \
  _uttserr_packargerr((Part), (Code), (Index))

#define uttserr_repack(Part, Err) \
  _uttserr_repackerr((Part), (Err))

/* Error logging API. */
#define UTTS_HAVE_LOG_API

/* error logging (only available if stdio or other io backend is available) */
int uttserr_setiospec(struct utts_iospec *pIO);
int uttserr_getiospec(struct utts_iospec *pIO);

/* pFileName, lineno, err, pMsgStr, pFormat, ... */
int uttserr_log(char const *, int, int, char const *, char const *, ...);

/* Driver backends. */
typedef enum utts_driver_type
{
  utts_driver_none        = 0, /* No driver. */
  utts_driver_void_type   = 1, /* A dummy driver to isolate the dispatch
                                * infrastructure during testing. */
  utts_driver_sapi_type   = 2, /* Windows SAPI driver using the Windows API C
                                * object macros to access ISpVoice and
                                * ISpStream. */
  utts_driver_nvda_type   = 3, /* Simple NVDA driver using dynamic loading of
                                * "nvdaControllerXX.dll" where XX is 32 or 64
                                * where appropriate. */
  utts_driver_espeak_type = 4, /* Driver using espeak for Linux. */
  utts_driver_user_type   = 5, /* A user driver implemented with user supplied
                                * callbacks. */
  utts_driver_type_count  = 5  /* Number of drivers. */
} utts_driver_type_e;

/* This is the structure for a single speech synthesis command. If (1) pString
 * is non-null, the command is taken to be a speech particle to be rendered and
 * spoken. If (2) pSpeech is null, the command is taken to be a silence command;
 * all speech will halt when the command is processed. */ 
typedef struct utts_speech_descriptor
{
  char const *pString;           /* Source string. */
  short volume, speed, pitch;    /* Ranges normalized between -32767 to 32767.
                                  * Mapped ranges and precision dependent on
                                  * driver's speech synthesis API. */
  void *pUserData;               /* Driver dependent, see driver documentation.
                                  * */
  void *pBackendConfigs;         /* Driver dependent, see driver documentation.
                                  * */
  void *pReserved1, *pReserved2; /* This way the ABI is futureproof against two
                                  * new address sized inputs. */
  
} utts_speech_descriptor_t;

/* Handle for an active driver. Typically only one of these is desired at a time
 * if the user program is well designed. */
typedef struct utts_driver
{
  utts_driver_type_e eType;      /* Identifies the driver. */
  int frameRate, bitWidth;       /* Output stream specs. */
  void *pUserData;               /* Userdata. Can be used for identification in
                                  * user program or for the user driver
                                  * implementation. */
  void *pReserved1, *pReserved2; /* This way the ABI is futureproof against two
                                  * new address sized inputs. */
} utts_driver_t;

/* Driver name API. */
#define UTTS_HAVE_ENUM_API

char const *utts_driver_type_name(utts_driver_type_e eType);
char const *utts_driver_name(utts_driver_t *pDriver);
utts_driver_type_e utts_driver_name_type(char const *pDriverName);

/* Start and stop. */
#define UTTS_HAVE_EXECUTE_API

int utts_boot(utts_driver_type_e ePreferred, utts_driver_t **ppDriver);
int utts_halt(utts_driver_t **ppDriver);

/* Cache hint control. */
#define UTTS_HAVE_HINT_API

/* Prepare a sample ahead of time if available through the driver and if
 * preferred by the driver. */
int utts_cstage(utts_speech_descriptor_t *pSpeech, utts_driver_t *pDriver);

/* Delete an entry from the cache if it's there. */
int utts_cunstage(utts_speech_descriptor_t *pSpeech, utts_driver_t *pDriver);

/* Purge the sample cache. */
int utts_cpurge(utts_driver_t *pDriver);

/* Speech control. */
#define UTTS_HAVE_CONTROL_API

/* Speak a speech sample. Creates a cache entry if preferred by the driver. */
int utts_speak(utts_speech_descriptor_t *pSpeech, utts_driver_t *pDriver);

/* Silence all speech. */
int utts_silence(utts_driver_t *pDriver);

/* Is speaking? */
int utts_isspeaking(utts_driver_t *pDriver);

/* Time API. */
#define UTTS_HAVE_TIMEUTC_API

/* Get the current UTC time from a monotonic source and copy it in to a timespec
 * structure. */
int utts_timeutc(utts_timespec_t *);

/* Render a UTC timestamp string from a timespec structure.*/
int utts_timeutctoa(char *, unsigned long, utts_timespec_t const *);

#define UTTS_HAVE_IO_API

/* utts_iospec::kind must be set to one of these. */
enum
{
  uttsio_stdiofile = 1,
  uttsio_stdiobuf = 2
};

struct utts_iospec
{
  int kind; /* kind of object to output to. */
  void *pData; /* FILE pointer or buffer pointer. */
  unsigned long sz; /* size of the buffer if applicable or zero. */
};

int utts_vioprintf(struct utts_iospec const *, char const *, va_list);
int utts_ioprintf(struct utts_iospec const *, char const *, ...);

#endif
