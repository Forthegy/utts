/*
   SipHash reference C implementation
   Copyright (c) 2016 Jean-Philippe Aumasson <jeanphilippe.aumasson@gmail.com>
   To the extent possible under law, the author(s) have dedicated all copyright
   and related and neighboring rights to this software to the public domain
   worldwide. This software is distributed without any warranty.
   You should have received a copy of the CC0 Public Domain Dedication along
   with
   this software. If not, see
   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

/* This file is modified from the original version (halfsiphash.c) to ensure C89
 * compatibility as well as to account for the same cross-platform
 * considerations of this project (uTTS). Dependency on stdint and stdio is
 * removed.
 * 2021-07-29 Forthegy <3 */

#include <assert.h>
#include <string.h>

#ifdef _SIPDEBUG
#include <stdio.h>
#endif

typedef unsigned char _sipbyte_t;
typedef unsigned long _sipword32_t;

/* default: SipHash-2-4 */
#ifndef cROUNDS
#define cROUNDS 2
#endif
#ifndef dROUNDS
#define dROUNDS 4
#endif

#define _SIPROTL(x, b) (_sipword32_t)(((x) << (b)) | ((x) >> (32 - (b))))

#define _SIPUINT32_C(x) ((_sipword32_t)(x))

#define _SIPU32TO8_LE(p, v)                                          \
  (p)[0] = (_sipbyte_t)((v));                                        \
  (p)[1] = (_sipbyte_t)((v) >> 8);                                   \
  (p)[2] = (_sipbyte_t)((v) >> 16);                                  \
  (p)[3] = (_sipbyte_t)((v) >> 24);

#define _SIPU8TO32_LE(p)                                             \
  (((_sipword32_t)((p)[0])) | ((_sipword32_t)((p)[1]) << 8) |        \
    ((_sipword32_t)((p)[2]) << 16) | ((_sipword32_t)((p)[3]) << 24))

#define _SIPROUND                                                    \
  do {                                                               \
    v0 += v1;                                                        \
    v1 = _SIPROTL(v1, 5);                                            \
    v1 ^= v0;                                                        \
    v0 = _SIPROTL(v0, 16);                                           \
    v2 += v3;                                                        \
    v3 = _SIPROTL(v3, 8);                                            \
    v3 ^= v2;                                                        \
    v0 += v3;                                                        \
    v3 = _SIPROTL(v3, 7);                                            \
    v3 ^= v0;                                                        \
    v2 += v1;                                                        \
    v1 = _SIPROTL(v1, 13);                                           \
    v1 ^= v2;                                                        \
    v2 = _SIPROTL(v2, 16);                                           \
  } while (0)

#if defined(_SIPDEBUG)
#define _SIPTRACE                                                    \
  do {                                                               \
    printf("(%3zu) v0 %08" PRIx32 "\n", inlen, v0);                  \
    printf("(%3zu) v1 %08" PRIx32 "\n", inlen, v1);                  \
    printf("(%3zu) v2 %08" PRIx32 "\n", inlen, v2);                  \
    printf("(%3zu) v3 %08" PRIx32 "\n", inlen, v3);                  \
  } while (0)
#else
#define _SIPTRACE
#endif

static int _halfsiphash(void const *in, size_t const inlen,
  void const *k, _sipbyte_t *out,
  size_t const outlen)
{
  _sipbyte_t const *ni = (_sipbyte_t const *)in;
  _sipbyte_t const *kk = (_sipbyte_t const *)k;

  assert((outlen == 4) || (outlen == 8));

  _sipword32_t v0 = 0;
  _sipword32_t v1 = 0;
  _sipword32_t v2 = _SIPUINT32_C(0x6c796765);
  _sipword32_t v3 = _SIPUINT32_C(0x74656462);
  _sipword32_t k0 = _SIPU8TO32_LE(kk);
  _sipword32_t k1 = _SIPU8TO32_LE(kk + 4);
  _sipword32_t m;
  int i;
  _sipbyte_t const *end = ni + inlen - (inlen % sizeof(_sipword32_t));
  int const left = inlen & 3;
  _sipword32_t b = ((_sipword32_t)inlen) << 24;
  v3 ^= k1;
  v2 ^= k0;
  v1 ^= k1;
  v0 ^= k0;

  if (outlen == 8)
    v1 ^= 0xee;

  for (; ni != end; ni += 4)
  {
    m = _SIPU8TO32_LE(ni);
    v3 ^= m;

    _SIPTRACE;
    for (i = 0; i < cROUNDS; ++i)
      _SIPROUND;

    v0 ^= m;
  }

  switch (left)
  {
  case 3:
    b |= ((_sipword32_t)ni[2]) << 16;
  case 2:
    b |= ((_sipword32_t)ni[1]) << 8;
  case 1:
    b |= ((_sipword32_t)ni[0]);
    break;
  case 0:
    break;
  }

  v3 ^= b;

  _SIPTRACE;
  for (i = 0; i < cROUNDS; ++i)
    _SIPROUND;

  v0 ^= b;

  if (outlen == 8)
    v2 ^= 0xee;
  else
    v2 ^= 0xff;

  _SIPTRACE;
  for (i = 0; i < dROUNDS; ++i)
    _SIPROUND;

  b = v1 ^ v3;
  _SIPU32TO8_LE(out, b);

  if (outlen == 4)
    return 0;

  v1 ^= 0xdd;

  _SIPTRACE;
  for (i = 0; i < dROUNDS; ++i)
    _SIPROUND;

  b = v1 ^ v3;
  _SIPU32TO8_LE(out + 4, b);

  return 0;
}
