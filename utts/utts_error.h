#ifndef _UTTS_ERROR_INCLUDED_
#define _UTTS_ERROR_INCLUDED_

/* Error return bitfields. */
enum
{
  _uttserr_usermask  = 0x000001FF, /* user information mask - 0th through 8th
                                    * bits set */
  _uttserr_usershft  = 0,          /* shift for the user information */
  _uttserr_userbits  = 9,          /* significant bits for the user information
                                    * */
  _uttserr_userbmask = 0x00001FF,  /* base mask (usermask >> usershft) */
  _uttserr_usersgnd  = 1,          /* sign extension occurs during retrieval */

  _uttserr_partmask  = 0x00007E00, /* part id mask - 9th through 14th bits set
                                    * */
  _uttserr_partshft  = 9,          /* shift for the part id */
  _uttserr_partbits  = 6,          /* significant bits for the part id */
  _uttserr_partbmask = 0x000003F,  /* base mask (partmask >> partshft) */
  _uttserr_partsgnd  = 0,          /* no sign extension occurs during retrieval
                                    * s*/

  _uttserr_codemask  = 0xFFFF8000, /* code mask - 15th through 31st bits set */
  _uttserr_codeshft  = 15,         /* shift for the error code */
  _uttserr_codebits  = 17,         /* significant bits for the code */
  _uttserr_codebmask = 0x07FFFFF,  /* base mask (codemask >> codeshft) */
  _uttserr_codesgnd  = 1           /* sign extension occurs during retrieval */
};

/* mask for sign bit */
#define _uttserr_gsgn(M) ((M) ^ ((M) >> 1))

/* sign extension with precomputed mask */
#define _uttserr_gsgnx(Value, M) \
  ((((Value) & (M)) ^ _uttserr_gsgn(M)) - _uttserr_gsgn(M))

/* Internal macros for extracting the error fields. uex, pex, and cex extract
 * the user, part, and code fields from the error that Err evaluates to
 * respectively.  */
#define _uttserr_uex(E) _uttserr_gsgnx((E) >> _uttserr_usershft, _uttserr_userbmask)
#define _uttserr_pex(E) (((E) >> _uttserr_partshft) & _uttserr_partbmask) /* unsigned */
#define _uttserr_cex(E) _uttserr_gsgnx((E) >> _uttserr_codeshft, _uttserr_codebmask)

/* Create an argument error from the given argument index. This consists of an
 * error whose part ID and code fields are 0 and whose user field is -(Index).
 * This can also be passed as the user part of another error to supply context.
 * The allowable indices are 1 through 63. If Index evaluates to a value that is
 * out of range, then the resulting error will not be interpreted as an argument
 * error so, for example, utts_isargerr(uttserr_argerr(64)) == 0 but
 * utts_isargerr(uttserr_argerr(2)) == 1. */
#define _uttserr_argerr(Index) (_uttserr_usermask & -(int)(Index))

/* Returns one if and only if Err evaluates to an error whose user field
 * represents an argument, otherwise 0. */
#define _uttserr_isargerr(Err) (_uttserr_getarg((Err)) != 0)

/* Get the argument number (1 through 63 inclusive) of an argument error, or 0
 * if the given error is not an argument error. */
static inline int _uttserr_getarg(int err)
{
  err = _uttserr_uex(err);
  return -((0 > err) & (-64 < err)) & -err;
}

/* Unpack the user, part ID, and code fields, with proper sign extension. The
 * user and code fields are signed 9 (resp. 17) bit integers, and the part ID
 * field is an unsigned 6 bit integer. */
#define _uttserr_getuser(Err) _uttserr_uex((Err)) /* signed */
#define _uttserr_getpart(Err) _uttserr_pex((Err)) /* unsigned */
#define _uttserr_getcode(Err) _uttserr_cex((Err)) /* signed */

/* Internal macros for packing the error fields. upk, ppk, and cpkk pack the
 * user, part, and code fields in new errors. These can be or'd together to
 * pack different combinations.  */
#define _uttserr_upk(U) ((((int)(U)) << _uttserr_usershft) & _uttserr_usermask)
#define _uttserr_ppk(P) ((((int)(P)) << _uttserr_partshft) & _uttserr_partmask)
#define _uttserr_cpk(C) ((((int)(C)) << _uttserr_codeshft) & _uttserr_codemask)

/* Pack the user, part ID, and code fields. The user and code fields are signed
 * 9 (resp. 17) bit integers, and the part ID field is an unsigned 6 bit
 * integer. */
#define _uttserr_user(User) _uttserr_upk((User))
#define _uttserr_part(Part) _uttserr_ppk((Part))
#define _uttserr_code(Code) _uttserr_cpk((Code))

/* pack part, code, and user parts for an error */
#define _uttserr_packerr(Part, Code, User) \
  ( _uttserr_user((User))                  \
  | _uttserr_part((Part))                  \
  | _uttserr_code((Code)))

/* pack part, code, and user parts for an error */
#define _uttserr_packargerr(Part, Code, Index) \
  _uttserr_packerr((Part), (Code), _uttserr_argerr((Index)))

#define _uttserr_repackerr(Part, Err) \
  ( (Err) & ~_uttserr_partmask        \
  | _uttserr_part((Part)))

#define _uttserr_stub(File, Line, Part, Name)                     \
    (uttserr_log((File), (Line),                                  \
        uttserr_pack((Part), uttserrcode_info, uttserrinfo_stub), \
        Name " is a stub!", NULL),                                \
    uttserr_pack((Part), uttserrcode_info, uttserrinfo_stub))

#define _uttserr_deprecated(File, Line, Part, Name)                     \
    (uttserr_log((File), (Line),                                        \
        uttserr_pack((Part), uttserrcode_info, uttserrinfo_deprecated), \
        Name " is deprecated. ", NULL),                                 \
    uttserr_pack((Part), uttserrcode_info, uttserrinfo_deprecated))

#endif
