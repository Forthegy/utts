#ifndef _UTTS_SYSTEM_IMPL_
#define _UTTS_SYSTEM_IMPL_

#include "system.h"

#if defined(UTTS_HAVE_STDTIME)

/* get UTC timespec portably between windows and posix. return 0 on success. */
static int _utts_timeutc(utts_timespec_t *pts)
{
#if defined(UTTS_WINDOWS)
  ULARGE_INTEGER s;
  FILETIME ft;
  ULONGLONG t;

  clearmem(&ft, sizeof(ft));
  (void)GetSystemTimeAsFileTime(&ft);

  s.LowPart = ft.dwLowDateTime;
  s.HighPart = ft.dwHighDateTime;

  t = s.QuadPart - 116444736000000000ULL;

  pts->tv_sec = (time_t)t / 10000000LL;
  pts->tv_nsec = ((long)(t % 10000000ULL)) * 100L;

  return 0;
#else /* posix ??? */
#if (0 <= _POSIX_TIMERS)
  if (0 == clock_gettime(CLOCK_REALTIME, pts))
    return 0;
#endif
#if (0 >= _POSIX_TIMERS)
  {
    struct timeval tv;

    if (0 == gettimeofday(&tv, NULL))
    {
      pts->tv_sec = tv.tv_sec;
      pts->tv_nsec = tv.tv_usec * 1000;
      return 0;
    }
  }
#endif
  return uttserr_pack(uttserrpart_time, uttserrcode_notime, 0);
#endif
}

static int _utts_timeutctoa(char *pBuf, unsigned long sz, utts_timespec_t const *pts)
{
#if defined(UTTS_WINDOWS)
  struct tm tm;
#endif
  unsigned long rc;
#if !defined(UTTS_NOSTDIO)
  unsigned long rc2;
#endif

  rc = 0;
#if defined(UTTS_WINDOWS)
  clearmem(&tm, sizeof(tm));
  if (0 == gmtime_s(&tm, &pts->tv_sec))
    rc = (unsigned long)strftime(pBuf, sz, "%Y-%m-%d %H:%M:%S", &tm);
#else
  rc = (unsigned long)strftime(pBuf, sz,
    "%Y-%m-%d %H:%M:%S", gmtime(&pts->tv_sec));
#endif
  if (rc < 0)
    return uttserr_pack(uttserrpart_time, uttserrcode_strftime, rc);
#if !defined(UTTS_NOSTDIO)
  else if (rc >= sz)
    return rc;

  rc2 = snprintf(pBuf + rc, sz - rc,
    ".%06ld UTC", (long)pts->tv_nsec / 1000L);
  if (rc2 < 0)
    return uttserr_pack(uttserrpart_time, uttserrcode_xprintf, rc2);
  else if (rc2 >= sz - rc)
    return rc2;

  return rc + rc2;
#else
  return rc;
#endif
}
#else
static int _utts_timeutc(utts_timespec_t *pts)
{
  (void)pts;
  return uttserr_pack(uttserrpart_time, uttserrcode_notime, 0);
}

static int _utts_timeutctoa(char *pBuf, unsigned long sz, utts_timespec_t const *pts)
{
  (void)(pBuf, sz, pts);
  return uttserr_pack(uttserrpart_time, uttserrcode_notime, 0);
}
#endif

int utts_timeutc(utts_timespec_t *pts)
{
#if defined(UTTS_HAVE_STDTIME)
  return _utts_timeutc(pts);
#else
  (void)pts;
  return uttserr_pack(uttserrpart_time, uttserrcode_notime, 0);
#endif
}

int utts_timeutctoa(char *pBuf, unsigned long sz, utts_timespec_t const *pts)
{
#if defined(UTTS_HAVE_STDTIME)
  return _utts_timeutctoa(pBuf, sz, pts);
#else
  (void)(pBuf, sz, pts);
  return uttserr_pack(uttserrpart_time, uttserrcode_notime, 0);
#endif
}

#endif